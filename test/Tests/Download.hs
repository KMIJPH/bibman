module Tests.Download (downloadSpec) where

import Bibman (extractUrl, readFileUTF8)
import Test.Hspec

downloadSpec :: Spec
downloadSpec = do
  describe "find dl url" $ do
    it "does nothing" $ do
      body <- readFileUTF8 "test/data/doi2pdf.html"
      extractUrl body `shouldBe` Just "https://the-url.pdf#view=FitH"

-- File Name: Date.hs
-- Description: Date stuff
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 08 Jul 2024 16:54:35
-- Last Modified: 12 Jul 2024 16:30:32

module Bibman.Pandoc.Date (issuedToYear, standardizeSeparator) where

import Data.Text (Text)
import Data.Text qualified as T
import Text.Pandoc (MetaValue (MetaString))

standardizeSeparator :: Text -> Text
standardizeSeparator = T.replace "/" "-" . T.replace "." "-" . T.replace "," "-"

-- | extracts the year from the 'issued' metavalue
issuedToYear :: MetaValue -> Text
issuedToYear (MetaString s) = case T.splitOn "-" (standardizeSeparator s) of
  [] -> ""
  [y] -> y
  (y : _) -> y
issuedToYear _ = ""

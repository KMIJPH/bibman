-- File Name: Config.hs
-- Description: Configuration
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 25 Apr 2024 10:35:16
-- Last Modified: 29 Aug 2024 15:22:41

module Bibman.CLI.Config
  ( readConfig,
    AppConfig (..),
    Order (..),
    parseFilter,
    defaultConfig,
    Filter (..),
  )
where

import Bibman.CLI.Sys (dieWithMsg)
import Control.Monad (when)
import Data.Configurator (Worth (..), load, lookupDefault)
import Data.Configurator.Types (Config, Name)
import Data.List (intercalate)
import Data.Text (Text)
import System.Directory (XdgDirectory (..), doesFileExist, getXdgDirectory)
import System.FilePath ((</>))

data Order = Author | Year | Title | Container | Publisher deriving (Eq, Enum, Bounded)

instance Show Order where
  show Author = "author"
  show Year = "year"
  show Title = "title"
  show Container = "container"
  show Publisher = "publisher"

orders :: [Order]
orders = [minBound ..]

data Filter
  = AccessNote
  | FixDate
  | NewCitekey
  | ReplaceEmpty
  | ToWebpage
  deriving (Eq, Read, Enum, Bounded, Ord)

instance Show Filter where
  show AccessNote = "accessNote"
  show FixDate = "fixDate"
  show NewCitekey = "newCitekey"
  show ReplaceEmpty = "replaceEmpty"
  show ToWebpage = "toWebpage"

filters :: [Filter]
filters = [minBound ..]

parseFilter :: String -> Either String Filter
parseFilter "accessNote" = Right AccessNote
parseFilter "newCitekey" = Right NewCitekey
parseFilter "replaceEmpty" = Right ReplaceEmpty
parseFilter "fixDate" = Right FixDate
parseFilter "toWebpage" = Right ToWebpage
parseFilter _ = Left $ "Available filters: " <> intercalate ", " (map show filters)

data AppConfig = AppConfig
  { resourceFormats :: [Text],
    bibPath :: FilePath,
    dlProvider :: Text,
    defFilters :: [Filter],
    citekeyOrder :: [Order],
    citekeyLengths :: [Int],
    citekeySep :: Text,
    colOrder :: [Order],
    colWidths :: [Float],
    openCmd :: Text,
    openTerminal :: Bool,
    editCmd :: Text,
    editTerminal :: Bool,
    doiUrlCmd :: Text,
    doiUrlTerminal :: Bool,
    clipboardCmd :: Text
  }
  deriving (Eq, Show)

defaultConfig :: AppConfig
defaultConfig =
  AppConfig
    { resourceFormats = ["pdf", "epub"],
      bibPath = "",
      dlProvider = "",
      defFilters = [],
      citekeyOrder = [Author, Year, Title],
      citekeyLengths = [20, 4, 20],
      citekeySep = "-",
      colOrder = [Author, Year, Title],
      colWidths = [15, 4, 80],
      openCmd = "",
      openTerminal = False,
      editCmd = "",
      editTerminal = False,
      doiUrlCmd = "",
      doiUrlTerminal = False,
      clipboardCmd = ""
    }

readConfig :: IO AppConfig
readConfig = do
  confDir <- getXdgDirectory XdgConfig "bibman"
  let configFile = confDir </> "bibman.conf"
  exists <- doesFileExist configFile
  if exists
    then
      ( do
          cfg <- load [Optional configFile]
          resourceFormats <- lookupDefault (resourceFormats defaultConfig) cfg "resourceFormats"
          bibPath <- lookupDefault (bibPath defaultConfig) cfg "bibPath"
          dlProvider <- lookupDefault (dlProvider defaultConfig) cfg "dlProvider"
          defFilters <- lookupFilter cfg "filters" defaultConfig
          citekeyOrder <- lookupOrderList cfg "citekey.order" defaultConfig
          citekeyLengths <- lookupDefault (citekeyLengths defaultConfig) cfg "citekey.lengths"
          citekeySep <- lookupDefault (citekeySep defaultConfig) cfg "citekey.sep"
          colOrder <- lookupOrderList cfg "tui.columns" defaultConfig
          colWidths <- lookupDefault (colWidths defaultConfig) cfg "tui.columnWidths"
          openCmd <- lookupDefault (openCmd defaultConfig) cfg "tui.open.cmd"
          openTerminal <- lookupDefault (openTerminal defaultConfig) cfg "tui.open.terminal"
          editCmd <- lookupDefault (editCmd defaultConfig) cfg "tui.edit.cmd"
          editTerminal <- lookupDefault (editTerminal defaultConfig) cfg "tui.edit.terminal"
          doiUrlCmd <- lookupDefault (doiUrlCmd defaultConfig) cfg "tui.doiUrl.cmd"
          doiUrlTerminal <- lookupDefault (doiUrlTerminal defaultConfig) cfg "tui.doiUrl.terminal"
          clipboardCmd <- lookupDefault (clipboardCmd defaultConfig) cfg "tui.clipboardCmd"
          dir <- case bibPath of
            "" -> do
              getXdgDirectory XdgData "bibman"
            _ -> pure bibPath
          let conf =
                AppConfig
                  resourceFormats
                  dir
                  dlProvider
                  defFilters
                  citekeyOrder
                  citekeyLengths
                  citekeySep
                  colOrder
                  colWidths
                  openCmd
                  openTerminal
                  editCmd
                  editTerminal
                  doiUrlCmd
                  doiUrlTerminal
                  clipboardCmd
          when (length citekeyOrder /= length citekeyLengths) $ dieWithMsg "[CONFIG]: citekey.order must be the same length as citekey.lengths"
          when (length colOrder /= length colWidths) $ dieWithMsg "[CONFIG]: tui.columnWidths must be the same length as tui.columns"
          pure conf
      )
    else do
      bibpath <- getXdgDirectory XdgData "bibman"
      pure $ defaultConfig {bibPath = bibpath}

-- | parses the order list
lookupOrderList :: Config -> Name -> AppConfig -> IO [Order]
lookupOrderList cfg name def = do
  orderStrings <- lookupDefault (map show (citekeyOrder def)) cfg name
  let parsedOrders = traverse (`parseOrder` name) orderStrings
  case parsedOrders of
    Right ords -> pure ords
    Left errorMsg -> do
      dieWithMsg errorMsg
      pure []
  where
    parseOrder :: String -> Name -> Either String Order
    parseOrder "author" _ = Right Author
    parseOrder "year" _ = Right Year
    parseOrder "title" _ = Right Title
    parseOrder "publisher" _ = Right Publisher
    parseOrder "container" _ = Right Container
    parseOrder o n = Left $ "[CONFIG]: Unknown type in " <> show n <> ": " <> show o <> ". Should be one/more of: " <> show orders

-- | parses filters
lookupFilter :: Config -> Name -> AppConfig -> IO [Filter]
lookupFilter cfg name def = do
  filterStrings <- lookupDefault (map show (defFilters def)) cfg name
  if null filterStrings
    then pure []
    else do
      let parsedFilters = traverse parseFilter filterStrings
      case parsedFilters of
        Right fs -> pure fs
        Left errorMsg -> do
          dieWithMsg errorMsg
          pure []

# bibman

![](./etc/screenshot.png)

`bibman` is a CLI/TUI bibliography manager.

It does not use a database but stores every bibliographic entry as a separate
text file. The entry's associated resource (e.g. pdf, epub, ...) is stored
using the same name. This should allow for easy manual editing/adding of files.

Under the hood, [the pandoc library](https://pandoc.org/) is used to read and
write bibliography entries, which makes this application format-agnostic, as it
can 'read from' and 'write to' all bibliography formats that pandoc
can handle:

```
(← = conversion from; → = conversion to; ↔︎ = conversion from and to)
↔︎ BibTeX
↔︎ BibLaTeX
↔︎ CSL JSON
↔︎ CSL YAML
← RIS
← EndNote XML
```

Format-agnostic means, that bibliography entries can be stored under any format
that pandoc can convert from.

## Usage

run `bibman <COMMAND> --help` to get more info
```
CLI/TUI bibliography manager

Usage: bibman COMMAND

Available options:
  -h,--help                Show this help text
  -v,--version             Show version

Available commands:
  add
  export
  tui
  tree
  list
  citekey
```

### Configuration file

The configuration file is read from `XDG_CONFIG_HOME/bibman/bibman.conf`.
For an example see [here](./etc/bibman.conf).

### Adding entries

#### via DOI

`bibman` can search `doi.org` and request a BibLaTeX entry for a given DOI.

e.g:
```sh
bibman add "10.1002/andp.19163540702" +papers/relevant # add to the <bibPath>/papers/relevant folder
bibman add "10.1002/andp.19163540702" ~/test.json # add to the file ~/test.json
cat refs.bib | bibman add - +papers/relevant # add the references in refs.bib to the <bibPath>/papers/relevant
cat refs.json | bibman add --from csljson - +papers/relevant # add the references in refs.json to the <bibPath>/papers/relevant
```

Passing the `-d` flag will try to download the associated pdf from `dlProvider`
by appending the DOI (i.e. `<dlProvider>/DOI`).  

### Exporting

```sh
bibman export > bib.bib  # exports the whole bibliography to one file
bibman export +papers/relevant > bib.bib  # exports the folder papers/relevant to one file
bibman export ~/test.json > bib.bib  # converts the .json file to a .bib file
cat refs.bib | bibman export --to csljson - > bib.json  # reads from STDIN and writes to json
```

### TUI

`bibman` provides a TUI to search/edit the bibliography entries and to open their associated resource.

```sh
bibman tui # opens the tui
bibman tui +papers/relevant # opens the tui for the papers/relevant folder
bibman tui ~/bib.json # opens the tui for a bibliography file
```

Press `?` to show a help page with keybindings.

## Filters (add, export)

Entries can be modified using filters with the flag `-F` (can be passed
multiple times). These are currently hardcoded functions to manipulate an
entry. Currently no documentation is provided for them. You will need to check
[the source code](https://codeberg.org/KMIJPH/bibman/src/branch/main/src/Bibman/CLI/Filter.hs).

```bash
cat refs.bib | bibman add - +papers/relevant -F <somefilter>
```

```bash
cat refs.bib | bibman export - -F <somefilter>
```

## Selectors (export, tui)

`bibman` features a very simple selector syntax. It lets you select entries based on their [CSL JSON keys](https://github.com/citation-style-language/schema/blob/master/schemas/input/csl-data.json).

Examples:

Select all books
```bash
bibman export -s "type == book"
```

Select books or journal-articles
```bash
bibman export -s "type == book || type == journal-article"
```

Select entries that have `Jane Doe` in their author list
```bash
bibman export -s "author ~= 'Jane Doe'"
```

Select everything but books
```bash
bibman export -s "! type == book"
```

Select books that were released after 2008
```bash
bibman tui -s "type == book && issued >= 2009"
```

Operators: 

| Operator     | Meaning        |
| :---:        |     :---       |
| ==           | equals         |
| ~=           | contains       |
| >            | greater        |
| >=           | greater equals |
| <            | less           |
| <=           | less equals    |
| &&           | logical AND    |
| \|\|         | logical OR     |
| !            | logical NOT    |

## Building

```sh
cabal build
```

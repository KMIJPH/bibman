{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE TemplateHaskell #-}

-- File Name: Events.hs
-- Description: TUI events
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 10 Jul 2024 11:14:35
-- Last Modified: 18 Jul 2024 12:00:20

module Bibman.TUI.Events
  ( mainEvent,
    helpEvent,
    sortEvent,
    filterEvent,
    exportEvent,
    runCommandEvent,
  )
where

import Bibman.CLI (AppConfig (..), getResource, openWith, refsToFile)
import Bibman.Pandoc (Reference, To, fromExtension, getReferencesByField, readPandoc)
import Bibman.TUI.Data
  ( AppState (..),
    FileAction (..),
    KeyBind (..),
    Row (..),
    Sorter (..),
    buildCmd,
    eventToKeybind,
    filesToRows,
    safeIndex,
    sortEntries,
    updateEntries,
    updateEntry,
  )
import Brick (get, put, suspendAndResume')
import Brick.Main qualified as M
import Brick.Types qualified as T
import Brick.Widgets.List qualified as L
import Control.Exception (SomeException, try)
import Control.Monad.IO.Class (MonadIO (liftIO))
import Data.Either (rights)
import Data.List (nub)
import Data.Map qualified as Map
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import Data.Text qualified as Text
import Data.Vector qualified as Vec
import Graphics.Vty qualified as V
import Lens.Micro (Lens', lens, (%~), (&), (^.))
import Lens.Micro.Mtl (use, (%=), (.=))
import Lens.Micro.TH (makeLensesFor)
import Text.Pandoc (MetaValue (MetaString))

makeLensesFor
  [ ("_config", "config"),
    ("_currentSort", "currentSort"),
    ("_doiUrlWithInput", "doiUrlWithInput"),
    ("_doiUrlWithPrompt", "doiUrlWithPrompt"),
    ("_editWithInput", "editWithInput"),
    ("_editWithPrompt", "editWithPrompt"),
    ("_entries", "entries"),
    ("_errorMessage", "errorMessage"),
    ("_exportInput", "exportInput"),
    ("_exportPrompt", "exportPrompt"),
    ("_fileWithInput", "fileWithInput"),
    ("_fileWithPrompt", "fileWithPrompt"),
    ("_filterInput", "filterInput"),
    ("_filterPrompt", "filterPrompt"),
    ("_helpText", "helpText"),
    ("_keybindList", "keybindList"),
    ("_showHelp", "showHelp"),
    ("_sortPressed", "sortPressed"),
    ("_view", "view")
  ]
  ''AppState

mainEvent :: V.Event -> T.EventM () AppState ()
mainEvent ev = do
  errorMessage .= ""
  helpText .= Nothing
  case eventToKeybind ev of
    Just Down -> view %= L.listMoveBy 1
    Just Down10 -> view %= L.listMoveBy 10
    Just Up -> view %= L.listMoveBy (-1)
    Just Up10 -> view %= L.listMoveBy (-10)
    Just GotoBeginning -> view %= L.listMoveToBeginning
    Just GotoEnd -> view %= L.listMoveToEnd
    Just Quit -> M.halt
    Just Filter -> filterPrompt .= True
    Just ExportEntries -> exportPrompt .= True
    Just Help -> showHelp .= True
    Just OpenEntry -> runCommandEvent Open ev
    Just EditEntry -> runCommandEvent Edit ev
    Just OpenWeb -> runCommandEvent DoiUrl ev
    Just Citekey -> runCommandEvent Clipboard ev
    Just ToggleMark -> markEntry
    Just UnmarkAll -> changeMarkedState False
    Just MarkAll -> changeMarkedState True
    Just Sort -> do
      helpText .= Just "sort: (a)uthor (t)itle (y)ear"
      sortPressed .= True
    _ -> pure ()
  where
    -- toggles marked status for a single entry
    markEntry :: T.EventM () AppState ()
    markEntry = do
      st <- get
      let list = st ^. view
          currentIdx = L.listSelected list
          elements = Vec.toList $ L.listElements list
      case currentIdx of
        Just idx -> do
          let selectedRow = safeIndex elements idx
          case selectedRow of
            Just row -> do
              let bool = not (marked row)
                  updatedRow = row {marked = bool}
                  updatedElements = take idx elements ++ [updatedRow] ++ drop (idx + 1) elements
                  updatedList = Vec.fromList updatedElements
                  entries' = st ^. entries
              view %= L.listReplace updatedList (Just idx)
              entries .= updateEntry entries' (p row) updatedRow
            Nothing -> pure ()
        Nothing -> pure ()

    -- changes marked state for all entries in view
    changeMarkedState :: Bool -> T.EventM () AppState ()
    changeMarkedState bool = do
      st <- get
      let list = st ^. view
          currentIdx = list ^. L.listSelectedL
          elements = L.listElements list
          updatedList = fmap (\r -> r {marked = bool}) elements
          entries' = st ^. entries
      view %= L.listReplace updatedList currentIdx
      entries .= updateEntries entries' (map (\e -> (p e, e)) $ Vec.toList updatedList)

-- | handles key events in the help view
helpEvent :: V.Event -> T.EventM n AppState ()
helpEvent ev = do
  case ev of
    V.EvKey (V.KChar 'j') [] -> keybindList %= L.listMoveBy 1
    V.EvKey (V.KChar 'k') [] -> keybindList %= L.listMoveBy (-1)
    V.EvKey V.KDown [] -> keybindList %= L.listMoveBy 1
    V.EvKey V.KUp [] -> keybindList %= L.listMoveBy (-1)
    _ -> showHelp .= False

-- | handles the sort key events and sorts the list
sortEvent :: V.Event -> T.EventM () AppState ()
sortEvent ev = do
  case ev of
    V.EvKey (V.KChar 'a') [] -> sorter Author
    V.EvKey (V.KChar 't') [] -> sorter Title
    V.EvKey (V.KChar 'y') [] -> sorter Year
    V.EvKey V.KBS [] -> filterInput %= Text.reverse . Text.drop 1 . Text.reverse
    _ -> do
      helpText .= Nothing
      sortPressed .= False
  where
    sorter :: Sorter -> T.EventM () AppState ()
    sorter s = do
      st <- get
      sortedList <-
        ( if st ^. currentSort == s
            then pure $ reverse (Vec.toList $ st ^. view . L.listElementsL)
            else pure $ sortEntries s (Vec.toList $ st ^. view . L.listElementsL)
          )
      put $ st & view %~ L.listReplace (Vec.fromList sortedList) (Just 0)
      helpText .= Nothing
      currentSort .= s
      sortPressed .= False

-- | handles filter events
filterEvent :: V.Event -> T.EventM () AppState ()
filterEvent ev = do
  case ev of
    V.EvKey V.KEsc [] -> filterPrompt .= False >> filterInput .= ""
    V.EvKey V.KEnter [] -> do
      runFilter
      filterPrompt .= False
    V.EvKey (V.KChar c) [] -> do
      filterInput %= (`Text.snoc` c)
      runFilter
    V.EvKey V.KBS [] -> do
      filterInput %= Text.reverse . Text.drop 1 . Text.reverse
      runFilter
    _ -> pure ()
  where
    runFilter :: T.EventM () AppState ()
    runFilter = do
      st <- get
      let original = st ^. entries
          filterStr = st ^. filterInput
          sort = st ^. currentSort
      view
        %= L.listReplace
          (Vec.fromList $ sortEntries sort (filterRows filterStr (concat $ Map.elems original)))
          (Just 0)

    -- case insensitive filtering
    filterRows :: Text -> [Row] -> [Row]
    filterRows txt' =
      filter
        ( \(Row {..}) ->
            Text.toLower txt' `Text.isInfixOf` Text.toLower a
              || Text.toLower txt' `Text.isInfixOf` Text.toLower ti
              || Text.toLower txt' `Text.isInfixOf` Text.toLower yr
              || any (\s -> Text.toLower txt' `Text.isInfixOf` Text.toLower s) as -- filter any author in the list
        )

-- | exports marked entries
exportEvent :: V.Event -> T.EventM n AppState ()
exportEvent ev = do
  case ev of
    V.EvKey V.KEsc [] -> exportPrompt .= False >> exportInput .= ""
    V.EvKey V.KEnter [] -> do
      exportPrompt .= False
      exporter
    V.EvKey (V.KChar c) [] -> exportInput %= (`Text.snoc` c)
    V.EvKey V.KBS [] -> exportInput %= Text.reverse . Text.drop 1 . Text.reverse
    _ -> pure ()
  where
    exporter :: T.EventM n AppState ()
    exporter = do
      st <- get
      let exportStr = st ^. exportInput
          exportPath = Text.unpack exportStr
          to = fromExtension (Text.unpack exportStr) :: To
      refs <- liftIO $ getMarked st
      case refs of
        [] -> errorMessage .= "no entry selected"
        _ -> do
          res <- liftIO $ ioToEither $ refsToFile [] to refs exportPath
          case res of
            Left errMsg -> errorMessage .= Text.pack (show errMsg)
            Right _ -> helpText .= Just ("exported to " <> exportStr)

runCommandEvent :: FileAction -> V.Event -> T.EventM () AppState ()
runCommandEvent act ev = do
  conf <- use config
  p <- use prompt
  ( if p
      then
        ( case ev of
            V.EvKey V.KEsc [] -> prompt .= False >> command .= ""
            V.EvKey (V.KChar c) [] -> command %= (`Text.snoc` c)
            V.EvKey V.KBS [] ->
              command %= Text.reverse . Text.drop 1 . Text.reverse
            V.EvKey V.KEnter [] ->
              do
                cmd <- use command
                executeCommand cmd
                prompt .= False
            _ -> pure ()
        )
      else
        ( case commandSetInConfig conf of
            "" -> prompt .= True
            c -> executeCommand c
        )
    )
  where
    commandSetInConfig :: AppConfig -> Text
    commandSetInConfig = case act of
      Open -> openCmd
      Edit -> editCmd
      DoiUrl -> doiUrlCmd
      Clipboard -> clipboardCmd

    prompt :: Lens' AppState Bool
    prompt = case act of
      Open -> fileWithPrompt
      Edit -> editWithPrompt
      DoiUrl -> doiUrlWithPrompt
      Clipboard -> lens (const False) const

    command :: Lens' AppState Text
    command = case act of
      Open -> fileWithInput
      Edit -> editWithInput
      DoiUrl -> doiUrlWithInput
      Clipboard -> lens (const "") const

    -- executes a command on the selected entry
    executeCommand :: Text -> T.EventM () AppState ()
    executeCommand cmd = do
      conf@AppConfig {..} <- use config
      maybeIdx <- use (view . L.listSelectedL)
      case maybeIdx of
        Nothing -> pure ()
        Just i -> do
          st <- get
          let Row {..} = Vec.toList (st ^. view . L.listElementsL) !! i
          case act of
            Clipboard -> do
              result <- liftIO $ openWith (Text.unpack $ buildCmd cmd ck) False
              case result of
                Left errMsg -> errorMessage .= Text.pack errMsg
                Right _ -> helpText .= Just "copied to clipboard"
            Edit -> do
              result <- suspendAndResume' $ openWith (Text.unpack $ buildCmd cmd (Text.pack p)) editTerminal
              case result of
                Left errMsg -> errorMessage .= Text.pack errMsg
                Right _ -> refresh st p
            DoiUrl -> do
              openUrl <- case ur of
                "" -> case di of
                  "" -> pure ""
                  _ -> pure $ "https://doi.org/" <> di
                _ -> pure ur
              if openUrl /= ""
                then
                  ( do
                      result <- liftIO $ openWith (Text.unpack $ buildCmd cmd openUrl) doiUrlTerminal
                      case result of
                        Left errMsg -> errorMessage .= Text.pack errMsg
                        Right _ -> pure ()
                  )
                else errorMessage .= "no url or doi found"
            Open -> do
              res <- liftIO $ getResource conf p ck
              case res of
                Just file -> do
                  result <- liftIO $ openWith (Text.unpack $ buildCmd cmd (Text.pack file)) openTerminal
                  case result of
                    Left errMsg -> errorMessage .= Text.pack errMsg
                    Right _ -> pure ()
                Nothing -> errorMessage .= "no resource found"

-- | refreshes rows for a single file
-- NOTE: can crash if wrongfully edited
refresh :: AppState -> FilePath -> T.EventM () AppState ()
refresh st path = do
  maybeIdx <- use (view . L.listSelectedL)
  rows <- liftIO $ filesToRows [path] []
  let updated = Map.insert path (concat $ Map.elems rows) (st ^. entries)
      sort = st ^. currentSort
  entries .= updated
  view .= L.list () (Vec.fromList (sortEntries sort $ concat $ Map.elems updated)) 1
  view %= L.listMoveTo (fromMaybe 0 maybeIdx)

-- | returns all marked references (also references that are not currently in view)
getMarked :: AppState -> IO [Reference]
getMarked st = do
  let citekeys = map (MetaString . ck) rows
      files = map p rows
  pandocs <- liftIO $ mapM readPandoc files
  let rs = rights pandocs
  pure $ nub (concatMap (\x -> getReferencesByField x "id" citekeys) rs)
  where
    rows = filter marked $ concat $ Map.elems (st ^. entries)

ioToEither :: IO a -> IO (Either SomeException a)
ioToEither = try

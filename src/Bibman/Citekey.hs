-- File Name: Citekey.hs
-- Description: Info generation / formatting
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 16 Jan 2024 11:54:41
-- Last Modified: 17 Jul 2024 01:15:52

module Bibman.Citekey (generateCitekey) where

import Bibman.CLI.Config (AppConfig (..))
import Bibman.CLI.Config qualified as C
import Bibman.Pandoc (Citation (..), authorEditorPublisher)
import Data.Char (isAlphaNum, isAscii)
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import Data.Text qualified as T

takeMax :: Int -> Text -> Text
takeMax n str
  | T.null str = ""
  | otherwise = T.take n str

formatPart :: Text -> Int -> Text
formatPart part len = takeMax len (alphaNum part)

alphaNum :: Text -> Text
alphaNum = T.filter isAlphaNum . T.filter isAscii -- probably not needed but who cares

-- | generates the citekey
generateCitekey :: AppConfig -> Citation -> Text
generateCitekey config entry@Citation {..} =
  T.intercalate sep $ zipWith formatOrder order lengths
  where
    order = citekeyOrder config
    lengths = citekeyLengths config
    sep = citekeySep config

    formatOrder :: C.Order -> Int -> Text
    formatOrder C.Author l = formatPart (authorEditorPublisher entry) l
    formatOrder C.Year l = formatPart (fromMaybe "no year" year) l
    formatOrder C.Title l = formatPart (fromMaybe "no title" title) l
    formatOrder C.Container l = formatPart (fromMaybe "no container" container) l
    formatOrder C.Publisher l = formatPart (fromMaybe "no publisher" publisher) l

module Main (main) where

import Test.Hspec
import Tests.Selector (selectorSpec)
import Tests.Read (readSpec)
import Tests.Download (downloadSpec)

main :: IO ()
main = hspec $ do
  readSpec
  selectorSpec
  downloadSpec

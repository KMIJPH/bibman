-- File Name: Args.hs
-- Description: CLI args
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 27 Mai 2024 18:04:34
-- Last Modified: 29 Aug 2024 15:40:34

module Bibman.CLI.Args
  ( treeParser,
    ckParser,
    tuiParser,
    exportParser,
    addParser,
    argsParser,
    Action (..),
    Args (..),
    versionOption,
    AddPath (..),
  )
where

import Bibman.CLI.Config (Filter (..), parseFilter)
import Bibman.CLI.Selector (Selector, parseSelector)
import Bibman.Pandoc (Format (parse), From (..), To (..))
import Data.Text (Text)
import Options.Applicative
  ( Alternative (many),
    Parser,
    ReadM,
    argument,
    command,
    eitherReader,
    fullDesc,
    header,
    help,
    helper,
    hidden,
    info,
    infoOption,
    long,
    metavar,
    option,
    short,
    str,
    subparser,
    switch,
    value,
  )

data Action = Add | Export | Tui | Tree | List | Citekey deriving (Show)

data AddPath = Absolute FilePath | Relative FilePath deriving (Show)

data Args = Args
  { action :: Action,
    boolArg1 :: Bool,
    boolArg2 :: Bool,
    stringArgs1 :: [Filter],
    from :: From,
    to :: To,
    selector :: Selector,
    optDoi :: Text,
    filePath :: AddPath
  }
  deriving (Show)

argsParser :: Parser Args
argsParser =
  subparser $
    command "add" (info (helper <*> addParser) (fullDesc <> header "add an entry (DOI, or read from STDIN) to a folder (PATH) in the bibliograhy"))
      <> command "export" (info (helper <*> exportParser) (fullDesc <> header "export a bibliography folder (PATH) or read from STDIN"))
      <> command "tui" (info (helper <*> tuiParser) (fullDesc <> header "open the TUI"))
      <> command "tree" (info (helper <*> treeParser) (fullDesc <> header "print bibliography tree"))
      <> command "list" (info (helper <*> pure (Args List False False [] Fbiblatex Tbiblatex [] "" (Relative ""))) (fullDesc <> header "list bibliography"))
      <> command "citekey" (info (helper <*> ckParser) (fullDesc <> header "print all citekeys"))

versionOption :: String -> Parser (a -> a)
versionOption v =
  infoOption
    ("bibman " <> v)
    ( long "version"
        <> short 'v'
        <> help "Show version"
        <> hidden
    )

inputReader :: ReadM From
inputReader = eitherReader parse

outputReader :: ReadM To
outputReader = eitherReader parse

filterReader :: ReadM Filter
filterReader = eitherReader parseFilter

selectorReader :: ReadM Selector
selectorReader = eitherReader parseSelector

addPath :: ReadM AddPath
addPath = eitherReader (Right <$> parseAddPath)
  where
    parseAddPath :: String -> AddPath
    parseAddPath ['-'] = Relative "-"
    parseAddPath ('+' : a) = Relative a
    parseAddPath a = Absolute a

addParser :: Parser Args
addParser = mkArgs <$> switches
  where
    switches =
      (,,,,,,)
        <$> switch (short 'd' <> long "dl" <> help "download the publication (using dlProvider)")
        <*> switch (short 'n' <> long "no-edit-retry" <> help "do not retry by editing the requested entry")
        <*> many (option filterReader (metavar "FILTER" <> short 'F' <> long "filter" <> help "filter to use for entries"))
        <*> option inputReader (metavar "FORMAT" <> short 'f' <> long "from" <> value Fbiblatex <> help "input format")
        <*> option outputReader (metavar "FORMAT" <> short 't' <> long "to" <> value Tbiblatex <> help "output format")
        <*> argument str (metavar "<DOI>" <> value "-" <> help "DOI to add (- for STDIN)")
        <*> argument addPath (metavar "<PATH>" <> value (Relative "") <> help "file or +<directory relative to bibPath>")

    mkArgs :: (Bool, Bool, [Filter], From, To, Text, AddPath) -> Args
    mkArgs (d, n, f, i, o, doi, path) = Args Add d n f i o [] doi path

exportParser :: Parser Args
exportParser = mkArgs <$> switches
  where
    switches =
      (,,,,)
        <$> many (option filterReader (metavar "FILTER" <> short 'F' <> long "filter" <> help "filter to use for entries"))
        <*> option inputReader (metavar "FORMAT" <> short 'f' <> long "from" <> value Fbiblatex <> help "input format")
        <*> option outputReader (metavar "FORMAT" <> short 't' <> long "to" <> value Tbiblatex <> help "output format")
        <*> option selectorReader (metavar "SELECTOR" <> short 's' <> long "selector" <> value [] <> help "selector")
        <*> argument addPath (metavar "<PATH>" <> value (Relative "") <> help "file, +<directory relative to bibPath> or '-' for STDIN")

    mkArgs :: ([Filter], From, To, Selector, AddPath) -> Args
    mkArgs (l, i, o, s, path) = Args Export False False l i o s "" path

tuiParser :: Parser Args
tuiParser =
  Args Tui False False [] Fbiblatex Tbiblatex
    <$> option selectorReader (metavar "SELECTOR" <> short 's' <> long "selector" <> value [] <> help "selector")
    <*> pure ""
    <*> argument addPath (metavar "<PATH>" <> value (Relative "") <> help "file or +<directory relative to bibPath>")

treeParser :: Parser Args
treeParser =
  Args Tree False False [] Fbiblatex Tbiblatex [] ""
    <$> argument addPath (metavar "<PATH>" <> value (Relative "") <> help "file or +<directory relative to bibPath>")

ckParser :: Parser Args
ckParser =
  Args Citekey False False [] Fbiblatex Tbiblatex [] ""
    <$> argument addPath (metavar "<PATH>" <> value (Relative "") <> help "file or +<directory relative to bibPath>")

-- File Name: Selector.hs
-- Description: Expression parsing and evaluating
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 13 Jul 2024 08:57:20
-- Last Modified: 14 Jul 2024 14:57:57

module Bibman.CLI.Selector (Operator (..), Logical (..), Cond (..), Selector, parseSelector, evaluateSelector) where

import Bibman.Pandoc (Reference, metaToText)
import Control.Applicative ((<|>))
import Control.Monad (void)
import Data.Map qualified as M
import Data.Maybe (fromMaybe, isNothing)
import Data.Text (Text)
import Data.Text qualified as T
import Text.Pandoc.Parsing (eof, many1, many1Till)
import Text.Parsec
  ( anyChar,
    char,
    choice,
    many,
    noneOf,
    optionMaybe,
    optional,
    parse,
    space,
    string,
    try,
  )
import Text.Parsec.String (Parser)

data Operator = Equals | Contains | Greater | GreaterEq | Less | LessEq deriving (Show, Eq)

data Logical = And | Or | End deriving (Show, Eq)

-- | negate, fieldname, operator, argument, logical
newtype Cond = Cond (Bool, Text, Operator, Text, Logical) deriving (Show, Eq)

-- | many chained conditions
type Selector = [Cond]

spaceSurrounded :: Parser a -> b -> Parser b
spaceSurrounded p r = do
  _ <- optional space
  _ <- p
  _ <- optional space
  pure r

quoted :: Parser Text
quoted = do
  _ <- char '\''
  content <- many1Till (quoted <|> escapedQuote <|> T.pack <$> many1 (noneOf ['\''])) (char '\'')
  pure $ T.concat content
  where
    escapedQuote :: Parser Text
    escapedQuote = T.pack <$> sequence [char '\\', char '\'']

operator :: Parser Operator
operator =
  choice
    [ try $ spaceSurrounded (string "==") Equals,
      try $ spaceSurrounded (string "~=") Contains,
      try $ spaceSurrounded (string ">=") GreaterEq,
      try $ spaceSurrounded (string ">") Greater,
      try $ spaceSurrounded (string "<=") LessEq,
      spaceSurrounded (string "<") Less
    ]

logical :: Parser Logical
logical =
  choice
    [ try $ spaceSurrounded (string "&&") And,
      spaceSurrounded (string "||") Or
    ]

condition :: Parser Cond
condition = do
  neg <- optionMaybe $ spaceSurrounded (char '!') ()
  f1 <- T.pack <$> many1Till anyChar (void space <|> eof)
  op <- operator
  f2 <- try quoted <|> T.pack <$> many1Till anyChar (void space <|> eof)
  l <- optionMaybe logical
  pure $ Cond (isNothing neg, f1, op, f2, fromMaybe End l)

selector :: Parser Selector
selector = many condition

parseSelector :: String -> Either String Selector
parseSelector input = case parse selector "" input of
  Right s -> Right s
  Left err -> Left $ "Could not parse selector!\n" <> show err

----------------------------------------------------------------------------------------------------------------------------

-- | evaluates a selector over references and returns the resulting references where conditions equal to true
evaluateSelector :: Selector -> [Reference] -> [Reference]
evaluateSelector conditions = filter (evalCs conditions)
  where
    comp :: Bool -> Operator -> Text -> Text -> Bool
    comp b Equals f v = if b then f == v else f /= v
    comp b Contains f v = if b then T.isInfixOf v f else not $ T.isInfixOf v f
    comp b Greater f v = if b then f > v else f <= v
    comp b GreaterEq f v = if b then f >= v else f < v
    comp b Less f v = if b then f < v else f >= v
    comp b LessEq f v = if b then f <= v else f > v

    evalC :: Cond -> Reference -> Bool
    evalC (Cond (neg, field, op, value, _)) row =
      case M.lookup field row of
        Just fieldValue -> comp neg op (metaToText fieldValue) value
        Nothing -> False

    evalCs :: [Cond] -> Reference -> Bool
    evalCs [] _ = True
    evalCs (Cond (neg, f, op, v, End) : _) row = evalC (Cond (neg, f, op, v, End)) row
    evalCs (Cond (neg, f, op, v, And) : cs) row = evalC (Cond (neg, f, op, v, And)) row && evalCs cs row
    evalCs (Cond (neg, f, op, v, Or) : cs) row = evalC (Cond (neg, f, op, v, Or)) row || evalCs cs row

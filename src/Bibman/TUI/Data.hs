-- File Name: Data.hs
-- Description: TUI data and util functions
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 27 Mai 2024 15:23:30
-- Last Modified: 29 Aug 2024 14:46:39

module Bibman.TUI.Data
  ( entryToRow,
    Row (..),
    Sorter (..),
    FileAction (..),
    AppState (..),
    KeyBind (..),
    filesToRows,
    dirToRows,
    sortEntries,
    normalAttr,
    buildCmd,
    safeIndex,
    normalAttrRev,
    authorAttr,
    eventToKeybind,
    updateEntry,
    yearAttr,
    highlightAttr,
    markedAttr,
    headerAttr,
    errorAttr,
    headerRow,
    columnAlignments,
    columnWidths,
    updateEntries,
    theMap,
    keybindText,
    initialState,
  )
where

import Bibman.CLI.Config (AppConfig)
import Bibman.CLI.FS (ls)
import Bibman.CLI.Selector (Selector, evaluateSelector)
import Bibman.Pandoc (Citation (..), authorEditorPublisher, getReferences, nameToText, readPandoc)
import Bibman.Pandoc.Citation (refToCitation)
import Brick.AttrMap qualified as A
import Brick.Util (on)
import Brick.Widgets.List qualified as L
import Brick.Widgets.Table qualified as Table
import Data.List (sort, sortBy)
import Data.Map (Map)
import Data.Map qualified as M
import Data.Map qualified as Map
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import Data.Text qualified as T
import Data.Vector qualified as Vec
import Graphics.Vty qualified as V
import System.Exit (die)

data Row = Row
  { a :: Text,
    as :: [Text],
    yr :: Text,
    ti :: Text,
    di :: Text,
    ur :: Text,
    ck :: Text,
    pb :: Text,
    cn :: Text,
    p :: FilePath,
    index :: Int,
    marked :: Bool
  }
  deriving (Eq, Show)

data FileAction = Open | Edit | DoiUrl | Clipboard deriving (Eq, Show)

data Sorter = Author | Title | Year deriving (Eq, Show)

data AppState = AppState
  { _view :: L.List () Row,
    _entries :: Map.Map FilePath [Row],
    _keybindList :: L.List () (Text, Text),
    _currentSort :: Sorter,
    _config :: AppConfig,
    _filterPrompt :: Bool,
    _filterInput :: Text,
    _fileWithPrompt :: Bool,
    _fileWithInput :: Text,
    _editWithPrompt :: Bool,
    _editWithInput :: Text,
    _doiUrlWithPrompt :: Bool,
    _doiUrlWithInput :: Text,
    _exportPrompt :: Bool,
    _exportInput :: Text,
    _showHelp :: Bool,
    _sortPressed :: Bool,
    _helpText :: Maybe Text,
    _errorMessage :: Text,
    _terminalWidth :: Int
  }

initialState :: Map FilePath [Row] -> AppConfig -> AppState
initialState rows conf =
  AppState
    { _view = L.list () (Vec.fromList $ sortEntries Author $ concat (M.elems rows)) 1,
      _entries = rows,
      _keybindList = L.list () (Vec.fromList keybindText) 1,
      _config = conf,
      _currentSort = Author,
      _filterPrompt = False,
      _filterInput = "",
      _fileWithPrompt = False,
      _fileWithInput = "",
      _editWithPrompt = False,
      _editWithInput = "",
      _doiUrlWithPrompt = False,
      _doiUrlWithInput = "",
      _exportPrompt = False,
      _exportInput = "",
      _showHelp = False,
      _sortPressed = False,
      _helpText = Nothing,
      _errorMessage = "",
      _terminalWidth = 200 -- is updated on start
    }

normalAttr :: A.AttrName
normalAttr = A.attrName "normal"

normalAttrRev :: A.AttrName
normalAttrRev = A.attrName "normal_rev"

authorAttr :: A.AttrName
authorAttr = A.attrName "author"

yearAttr :: A.AttrName
yearAttr = A.attrName "year"

highlightAttr :: A.AttrName
highlightAttr = A.attrName "highlighted"

markedAttr :: A.AttrName
markedAttr = A.attrName "marked"

headerAttr :: A.AttrName
headerAttr = A.attrName "header"

errorAttr :: A.AttrName
errorAttr = A.attrName "error"

headerRow :: Row
headerRow =
  Row
    { a = "Author",
      yr = "Year",
      ti = "Title",
      as = [],
      di = "",
      ur = "",
      p = "",
      pb = "Publisher",
      cn = "Container",
      marked = False,
      ck = "",
      index = 0
    }

columnAlignments :: [a] -> [Table.ColumnAlignment]
columnAlignments x = map (const Table.AlignLeft) x <> [Table.AlignLeft]

columnWidths :: Int -> [Float] -> [Int]
columnWidths totalWidth widths = [1] <> map percent widths
  where
    percent :: Float -> Int
    percent = round . (* (fromIntegral totalWidth / 99 :: Float))

theMap :: A.AttrMap
theMap =
  A.attrMap
    V.defAttr
    [ (normalAttr, V.defAttr `V.withForeColor` V.white),
      (normalAttrRev, V.black `on` V.white),
      (headerAttr, V.white `on` V.black),
      (highlightAttr, V.defAttr `V.withForeColor` V.blue),
      (markedAttr, V.defAttr `V.withForeColor` V.yellow),
      (errorAttr, V.defAttr `V.withForeColor` V.red),
      (authorAttr, V.defAttr `V.withForeColor` V.magenta),
      (yearAttr, V.defAttr `V.withForeColor` V.brightBlue)
    ]

data KeyBind
  = Help
  | Down
  | Up
  | Down10
  | Up10
  | GotoEnd
  | GotoBeginning
  | Filter
  | ToggleMark
  | MarkAll
  | UnmarkAll
  | OpenEntry
  | OpenWeb
  | Citekey
  | EditEntry
  | ExportEntries
  | Sort
  | Quit
  deriving (Eq, Enum, Bounded, Ord)

helpText :: KeyBind -> Text
helpText Citekey = "copy the citekey of the selected entry to the clipboard"
helpText Down = "go down"
helpText Down10 = "go down by 10"
helpText EditEntry = "edit the selected entry using a shell command"
helpText ExportEntries = "export marked entries to a file"
helpText Filter = "filter entries"
helpText GotoBeginning = "go to the beginning of the list"
helpText GotoEnd = "go to the end of the list"
helpText Help = "show this help page"
helpText MarkAll = "mark all entries (currently in view)"
helpText OpenEntry = "open the selected entry using a shell command"
helpText OpenWeb = "open the doi/url using a shell command"
helpText Quit = "quit"
helpText Sort = "sort entries"
helpText ToggleMark = "mark/unmark the selected entry"
helpText UnmarkAll = "unmark all entries (currently in view)"
helpText Up = "go up"
helpText Up10 = "go up by 10"

keyBindMap :: Map V.Event KeyBind
keyBindMap =
  M.fromList
    [ (V.EvKey (V.KChar '?') [], Help),
      (V.EvKey (V.KChar 'j') [], Down),
      (V.EvKey V.KDown [], Down),
      (V.EvKey (V.KChar 'k') [], Up),
      (V.EvKey (V.KChar 'c') [], Citekey),
      (V.EvKey V.KUp [], Up),
      (V.EvKey (V.KChar 'd') [V.MCtrl], Down10),
      (V.EvKey (V.KChar 'u') [V.MCtrl], Up10),
      (V.EvKey (V.KChar 'G') [], GotoEnd),
      (V.EvKey (V.KChar 'g') [], GotoBeginning),
      (V.EvKey (V.KChar 'f') [], Filter),
      (V.EvKey (V.KChar ' ') [], ToggleMark),
      (V.EvKey (V.KChar 'a') [V.MCtrl], MarkAll),
      (V.EvKey V.KEsc [], UnmarkAll),
      (V.EvKey V.KEnter [], OpenEntry),
      (V.EvKey (V.KChar 'w') [], OpenWeb),
      (V.EvKey (V.KChar 'e') [], EditEntry),
      (V.EvKey (V.KChar 'E') [], ExportEntries),
      (V.EvKey (V.KChar 's') [], Sort),
      (V.EvKey (V.KChar 'q') [], Quit)
    ]

eventToKeybind :: V.Event -> Maybe KeyBind
eventToKeybind ev = M.lookup ev keyBindMap

keybindText :: [(Text, Text)]
keybindText = zipWith (\k h -> (prettyEvent k, h)) kb ht
  where
    ks :: [KeyBind]
    ks = [minBound ..]

    kb = map (fromMaybe (V.EvKey (V.KChar ' ') []) . keybindToEvent) ks
    ht = map helpText ks

    keybindToEvent :: KeyBind -> Maybe V.Event
    keybindToEvent keybind = M.lookup keybind reversed
      where
        reversed :: Map KeyBind V.Event
        reversed = M.fromList $ map (\(k, v) -> (v, k)) (M.toList keyBindMap)

    prettyEvent :: V.Event -> Text
    prettyEvent (V.EvKey key mods) = prettyMods mods <> prettyKey key
    prettyEvent _ = ""

    prettyMods :: [V.Modifier] -> Text
    prettyMods [V.MCtrl] = "<Ctrl> + "
    prettyMods _ = ""

    prettyKey :: V.Key -> Text
    prettyKey (V.KChar ' ') = "<Space>"
    prettyKey (V.KChar c) = T.singleton c
    prettyKey V.KEnter = "<Enter>"
    prettyKey V.KDown = "<Down>"
    prettyKey V.KEsc = "<Esc>"
    prettyKey V.KUp = "<Up>"
    prettyKey _ = ""

----------------------------------------------------------------------------------------------------------------------------

sortAuthor :: Row -> Row -> Ordering
sortAuthor (Row a _ _ _ _ _ _ _ _ _ _ _) (Row b _ _ _ _ _ _ _ _ _ _ _) = compare a b

sortYear :: Row -> Row -> Ordering
sortYear (Row _ _ a _ _ _ _ _ _ _ _ _) (Row _ _ b _ _ _ _ _ _ _ _ _) = compare a b

sortTitle :: Row -> Row -> Ordering
sortTitle (Row _ _ _ a _ _ _ _ _ _ _ _) (Row _ _ _ b _ _ _ _ _ _ _ _) = compare a b

sortEntries :: Sorter -> [Row] -> [Row]
sortEntries s entries = case s of
  Author -> sortBy sortAuthor entries
  Title -> sortBy sortTitle entries
  Year -> sortBy sortYear entries

entryToRow :: Citation -> FilePath -> Int -> Row
entryToRow c@Citation {..} p i =
  Row
    { a = authorEditorPublisher c,
      as = maybe [] (map nameToText) authors,
      yr = fromMaybe "<empty>" year,
      ti = fromMaybe "<empty>" title,
      di = fromMaybe "" doi,
      ur = fromMaybe "" url,
      ck = fromMaybe "" citekey,
      pb = fromMaybe "<empty>" publisher,
      cn = fromMaybe "<empty>" container,
      p = p,
      index = i,
      marked = False
    }

-- | updates a single entry in the entries map
updateEntry :: Map FilePath [Row] -> FilePath -> Row -> Map FilePath [Row]
updateEntry m path row = case M.lookup path m of
  Just l -> M.insert path (updateByIndex l row) m
  Nothing -> m
  where
    updateByIndex :: [Row] -> Row -> [Row]
    updateByIndex rows newRow = map update rows
      where
        update r
          | index r == index newRow = newRow
          | otherwise = r

-- | updatess multiple entries in the entries map
updateEntries :: Map FilePath [Row] -> [(FilePath, Row)] -> Map FilePath [Row]
updateEntries = foldl (\acc (p, r) -> updateEntry acc p r)

-- | reads the references in multiple files and converts them to a map
-- each file can have multiple rows
filesToRows :: [FilePath] -> Selector -> IO (Map FilePath [Row])
filesToRows files sel = do
  (bibs, errors) <- processFiles files
  if null errors
    then do
      let rowsMap = Map.fromListWith (++) $ map (\(p, es) -> (p, zipWith (\i e -> entryToRow e p i) [0 ..] es)) bibs
      pure rowsMap
    else die (unlines errors)
  where
    processFiles :: [FilePath] -> IO ([(FilePath, [Citation])], [String])
    processFiles [] = pure ([], [])
    processFiles (file : rest) = do
      result <- readBibSel file
      case result of
        Right entries -> do
          (bibs, paths) <- processFiles rest
          pure ((file, entries) : bibs, paths)
        Left errorMsg -> do
          (bibs, paths) <- processFiles rest
          pure (bibs, ("Error in file " <> show file <> ": " <> errorMsg) : paths)

    readBibSel :: FilePath -> IO (Either String [Citation])
    readBibSel file = do
      entry <- readPandoc file
      case entry of
        Right pandoc -> do
          let refs = getReferences pandoc
              selected = evaluateSelector sel refs
              res = map refToCitation selected
          pure $ Right res
        Left err -> pure $ Left err

-- | reads all the references in a directory and converts them to a map
dirToRows :: AppConfig -> FilePath -> Selector -> IO (Map FilePath [Row])
dirToRows config path sel = do
  files <- ls config path
  filesToRows (sort files) sel

buildCmd :: Text -> Text -> Text
buildCmd cmd' args = T.replace "%s" args cmd'

safeIndex :: [a] -> Int -> Maybe a
safeIndex xs idx
  | idx >= 0 && idx < length xs = Just (xs !! idx)
  | otherwise = Nothing

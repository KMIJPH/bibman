#compdef bibman
# File Name: _bibman
# Description: Completion file for zsh
# Author: lukeflo
# Repository: codeberg.org/lukeflo/bibman
# License: GPL3
#
# Has to be put in a directory part of $fpath

_bibman ()
{
  local line state
  local -i ret
  local bibpath
  bibpath=$(grep -i bibpath "${XDG_CONFIG_HOME:-${HOME}/.config}/bibman/bibman.conf" | cut -d'"' -f 2)
    
  ret=1
    
  _arguments -C \
    '1: :->cmds' \
    '*:: :->args' \
    && ret=0

  case "$state" in
    cmds)
      _values "bibman <command>" \
        "add[add an entry (DOI, or read from STDIN) to a folder (PATH) in the bibliograhy]" \
        "export[export a bibliography folder (PATH) or read from STDIN]" \
        "tui[open the tui]" \
        "tree[print bibliography tree]" \
        "list[list bibliography]" \
        "citekey[get citekey for a given file]"
      ret=0
    ;;
    args)
      case "$line[1]" in
        add)
          _arguments \
            {-h,--help}'[Show help]' \
            {-d,--dl}'[download the publication (using dlProvider)]' \
            {-n,--no-edit-retry}'[do not retry by editing the requested .bib]' \
            '*:file name:_files -W "$bibpath"'
          ret=0
        ;;
        export)
          _arguments \
            {-h,--help}'[Show help]' \
            {-f,--filter}'[FILTER: filter to use for bibtex entries]:filter:' \
            {-t,--to}'[specify output FORMAT]:output format:(RIS)' \
            '*:file name:_files -W "$bibpath"'
          ret=0
        ;;
        tui)
          _arguments \
            {-h,--help}'[Show help]' \
            '*:file name:_files -W "$bibpath"'
          ret=0
        ;;
        tree)
          _arguments \
            {-h,--help}'[Show help]' \
            '*:file name:_files -W "$bibpath"'
          ret=0
        ;;
        citekey)
          _arguments \
            {-h,--help}'[Show help]' \
            '*:file name:_files -W "$bibpath"'
          ret=0
        ;;
        list)
          _arguments \
            {-h,--help}'[Show help]'
          ret=0
        ;;          
      esac
    ;;
  esac
}

-- File Name: Meta.hs
-- Description: Meta utility stuff
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 06 Jul 2024 11:35:50
-- Last Modified: 29 Aug 2024 11:22:40

module Bibman.Pandoc.Meta
  ( metaToText,
    setCitekey,
    getReferences,
    addReferences,
    updateReferences,
    mergePandocs,
    getReferencesByField,
    emptyReferences,
    noCite,
    setReferences,
    Reference,
  )
where

import Data.Map (Map)
import Data.Map qualified as M
import Data.Text (Text)
import Data.Text qualified as T
import Text.Pandoc
  ( Citation (..),
    CitationMode (NormalCitation),
    Meta (Meta),
    MetaValue (..),
    Pandoc (Pandoc),
    nullMeta,
  )
import Text.Pandoc.Builder (HasMeta (setMeta), cite, str)
import Text.Pandoc.Shared (blocksToInlines, stringify)

type Reference = Map Text MetaValue

metaToText :: MetaValue -> Text
metaToText (MetaInlines inl) = stringify inl
metaToText (MetaString s) = s
metaToText (MetaBool b) = if b then "True" else "False"
metaToText (MetaList s) = T.intercalate ", " (map metaToText s)
metaToText (MetaMap s) = T.intercalate ", " (map metaToText $ M.elems s)
metaToText (MetaBlocks bl) = metaToText $ MetaInlines $ blocksToInlines bl

-- | returns an empty pandoc (with the nocite wildcard set)
noCite :: Pandoc
noCite =
  setMeta
    "nocite"
    ( cite
        [ Citation
            { citationId = "*",
              citationPrefix = [],
              citationSuffix = [],
              citationMode = NormalCitation,
              citationNoteNum = 0,
              citationHash = 0
            }
        ]
        (str "[@*]")
    )
    (Pandoc nullMeta [])

-- | returns all the references in pandoc's meta
getReferences :: Pandoc -> [Reference]
getReferences (Pandoc (Meta m) b) = case M.lookup "references" m of
  Just (MetaList [MetaMap ref]) -> [ref]
  Just (MetaList (MetaMap ref : xs)) ->
    ref : getReferences (Pandoc (Meta $ M.fromList [("references", MetaList xs)]) b)
  _ -> []

-- | returns all the references that have a certain value for a key
getReferencesByField :: Pandoc -> Text -> [MetaValue] -> [Reference]
getReferencesByField p k comp =
  filter
    ( \m -> case M.lookup k m of
        Just v -> v `elem` comp
        Nothing -> False
    )
    (getReferences p)

-- | sets references to a pandoc
setReferences :: Pandoc -> [Reference] -> Pandoc
setReferences (Pandoc (Meta m) b) refs = Pandoc (Meta newMap) b
  where
    newMap = M.insert "references" (MetaList (map MetaMap refs)) m

-- | adds references to a pandoc
addReferences :: Pandoc -> [Reference] -> Pandoc
addReferences pandoc refs = setReferences pandoc (refs' <> refs)
  where
    refs' = getReferences pandoc

-- | updates all the references
updateReferences :: Pandoc -> (Reference -> Reference) -> Pandoc
updateReferences p@(Pandoc (Meta m) b) f = Pandoc (Meta newMap) b
  where
    refs = getReferences p
    updated = MetaList $ map (MetaMap . f) refs
    newMap = M.insert "references" updated m

-- | checks if references are empty
emptyReferences :: Pandoc -> Bool
emptyReferences p = null $ getReferences p

-- | merges multiple pandoc metas into one
mergePandocs :: [Pandoc] -> Pandoc
mergePandocs p = case p of
  [Pandoc (Meta m) b] -> Pandoc (Meta $ newMap m) b
  (Pandoc (Meta m) b : _) -> Pandoc (Meta $ newMap m) b
  [] -> Pandoc (Meta M.empty) []
  where
    refs = concatMap getReferences p
    newMap = M.insert "references" (MetaList $ map MetaMap refs)

-- | sets the citekey for a single reference
setCitekey :: Reference -> Text -> Reference
setCitekey m ck = M.insert "id" (MetaString ck) m

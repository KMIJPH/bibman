-- File Name: Export.hs
-- Description: Exporting
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 30 Mai 2024 13:32:49
-- Last Modified: 29 Aug 2024 11:54:34

module Bibman.CLI.Export (bibToStdout, refsToFile, eithersToEither) where

import Bibman.CLI.Config (AppConfig)
import Bibman.CLI.FS (ls)
import Bibman.CLI.Filter (FilterFunc, applyFilters)
import Bibman.CLI.Selector (Selector, evaluateSelector)
import Bibman.CLI.Sys (dieWithMsg, readFromStdin)
import Bibman.Pandoc (From, Reference, To, emptyReferences, getReferences, mergePandocs, noCite, readPandoc, readPandocFrom, setReferences)
import Data.Text (Text)
import Data.Text.IO qualified as TIO
import System.Directory (createDirectoryIfMissing)
import System.FilePath (takeDirectory)
import System.IO (IOMode (WriteMode), hClose, hSetEncoding, utf8, withFile)

-- | converts an entry
convert ::
  [FilterFunc] -> -- filters
  From -> -- input format
  To -> -- output format
  Text -> -- content
  Selector -> -- selector
  IO (Either String Text)
convert fns input output content sel = do
  entry <- readPandocFrom input content
  case entry of
    Right pandoc -> do
      if emptyReferences pandoc
        then pure $ Left "Empty references. Maybe you forgot to set the --from format"
        else do
          let refs = getReferences pandoc
              selected = evaluateSelector sel refs
          written <- applyFilters fns output (setReferences pandoc selected)
          case written of
            Right t -> pure $ Right t
            Left err -> pure $ Left err
    Left err -> pure $ Left err

eithersToEither :: [Either a b] -> Either a [b]
eithersToEither xs = do
  let converted = sequence xs
  case converted of
    Left err -> Left err
    Right results -> Right results

-- | prints the bibliography to stdout (path relative to bibliography)
bibToStdout ::
  AppConfig -> -- config
  [FilterFunc] -> -- filters
  From -> -- input format
  To -> -- output format
  FilePath -> -- path relative to bibliography
  Selector -> -- selector
  IO ()
bibToStdout config fns input output path sel = case path of
  "-" -> do
    content <- readFromStdin
    res <- convert fns input output content sel
    case res of
      Right t -> TIO.putStr t
      Left err -> dieWithMsg err
  _ -> do
    files <- ls config path
    mapped <- mapM readPandoc files -- from format is ignored here
    let result = eithersToEither mapped
    case result of
      Right pandocs -> do
        let pandoc = mergePandocs pandocs
            refs = getReferences pandoc
            selected = evaluateSelector sel refs
        res <- applyFilters fns output (setReferences pandoc selected)
        case res of
          Right t -> TIO.putStr t
          Left err -> dieWithMsg err
      Left err -> dieWithMsg err

-- | writes bibliography files to a new file
refsToFile ::
  [FilterFunc] -> -- filters
  To -> -- output format
  [Reference] -> -- references
  FilePath -> -- path to save to
  IO ()
refsToFile fns output refs path = do
  let pandoc = setReferences noCite refs
  res <- applyFilters fns output pandoc
  case res of
    Right t -> writeFileUTF8 t
    Left err -> dieWithMsg err
  where
    writeFileUTF8 :: Text -> IO ()
    writeFileUTF8 content = do
      let parent = takeDirectory path
      createDirectoryIfMissing True parent
      withFile path WriteMode $ \handle -> do
        hSetEncoding handle utf8
        TIO.hPutStr handle content
        hClose handle

-- File Name: Main.hs
-- Description: Bibliography manager
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 10 Jan 2024 18:54:02
-- Last Modified: 29 Aug 2024 15:49:14

module Main where

import Bibman
  ( Action (..),
    AddPath (..),
    AppConfig (..),
    Args (..),
    Citation (citekey),
    addToBib,
    app,
    argsParser,
    bibToStdout,
    dieWithMsg,
    dirToRows,
    eithersToEither,
    getReferences,
    initialState,
    ls,
    mergePandocs,
    readConfig,
    readPandoc,
    refToCitation,
    toFilterFunc,
    tree,
    versionOption,
  )
import Brick.Main qualified as M
import Control.Monad (void)
import Data.List (intercalate)
import Data.Maybe (catMaybes)
import Data.Set qualified as Set
import Data.Text qualified as T
import Data.Text.IO qualified as TIO
import Data.Version (showVersion)
import Options.Applicative (execParser, fullDesc, header, helper, info, (<**>))
import Paths_bibman (version)
import System.IO qualified as S

rmdups :: (Ord a) => [a] -> [a]
rmdups = removeDuplicates' Set.empty
  where
    removeDuplicates' _ [] = []
    removeDuplicates' seen (x : xs)
      | x `Set.member` seen = removeDuplicates' seen xs
      | otherwise = x : removeDuplicates' (Set.insert x seen) xs

main :: IO ()
main = do
  S.hSetEncoding S.stdout S.utf8
  Args {..} <- execParser (info (argsParser <**> helper <**> versionOption (showVersion version)) (fullDesc <> header "CLI/TUI bibliography manager" <> fullDesc))
  config <- readConfig
  let filterFuncs = rmdups $ defFilters config <> stringArgs1
  funcs <- mapM toFilterFunc filterFuncs

  (config', filePath') <- case filePath of
    Relative p -> pure (config, p)
    Absolute p -> pure (config {bibPath = p}, "")

  case action of
    Add -> do
      addToBib config' boolArg1 boolArg2 False optDoi filePath' funcs from to
    Export -> do
      bibToStdout config' funcs from to filePath' selector
    Tui -> do
      rows <- dirToRows config' filePath' selector
      void $ M.defaultMain app (initialState rows config')
    Tree -> do
      tree config' filePath'
    List -> do
      files <- ls config' filePath'
      putStr $ intercalate "\n" files
    Citekey -> do
      files <- ls config' filePath'
      res <- mapM readPandoc files
      case eithersToEither res of
        Right pandocs -> do
          let p = mergePandocs pandocs
              refs = getReferences p
              cits = map refToCitation refs
              cks = map citekey cits
          TIO.putStr $ T.intercalate "\n" (catMaybes cks)
        Left err -> dieWithMsg err
      pure ()

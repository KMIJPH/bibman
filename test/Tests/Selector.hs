module Tests.Selector (selectorSpec) where

import Bibman
  ( Cond (..),
    Logical (..),
    Operator (..),
    evaluateSelector,
    parseSelector,
  )
import Bibman.Pandoc (Reference)
import Data.Map qualified as M
import Test.Hspec
import Text.Pandoc (MetaValue (MetaString))

book1 :: Reference
book1 = M.fromList [("type", MetaString "book"), ("author", MetaString "Author Name")]

book2 :: Reference
book2 = M.fromList [("type", MetaString "book"), ("date", MetaString "2020-01-03")]

article :: Reference
article = M.fromList [("type", MetaString "journal-article"), ("author", MetaString "Author Name")]

refs1 :: [Reference]
refs1 = [book1, book2, article]

typeIs :: [Cond]
typeIs = [Cond (True, "type", Equals, "book", End)]

typeContains :: [Cond]
typeContains = [Cond (True, "type", Contains, "article", Or), Cond (True, "author", Equals, "Author Name", End)]

typeNot :: [Cond]
typeNot = [Cond (False, "type", Equals, "book", End)]

typeNotOr :: [Cond]
typeNotOr = [Cond (True, "type", Equals, "book", And), Cond (False, "author", Contains, "Author", End)]

selectorSpec :: Spec
selectorSpec = do
  describe "parse Selector" $ do
    it "parses Selectors" $ do
      parseSelector "type == book" `shouldBe` Right typeIs
      parseSelector "type ~= article || author == 'Author Name'" `shouldBe` Right typeContains
      parseSelector "! type == book" `shouldBe` Right typeNot
      parseSelector "type == book && ! author ~= Author" `shouldBe` Right typeNotOr

  describe "evaluate Selector" $ do
    it "evaluates Selectors" $ do
      evaluateSelector typeIs refs1 `shouldBe` [book1, book2]
      evaluateSelector typeContains refs1 `shouldBe` [book1, article]
      evaluateSelector typeNot refs1 `shouldBe` [article]

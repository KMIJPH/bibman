-- File Name: Add.hs
-- Description: Add to bibliography
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 30 Mai 2024 11:59:40
-- Last Modified: 29 Aug 2024 14:18:42

module Bibman.CLI.Add (addToBib) where

import Bibman.CLI.Config (AppConfig (bibPath))
import Bibman.CLI.Download (doi2bib, doi2pdf)
import Bibman.CLI.Export (eithersToEither)
import Bibman.CLI.FS (bibIsFile, ls, readFileUTF8, writeFileBinary, writeFileUTF8, writeTempFile)
import Bibman.CLI.Filter (FilterFunc, applyFilters, applyFilters')
import Bibman.CLI.Sys (dieWithMsg, openWith, readFromStdin)
import Bibman.Citekey (generateCitekey)
import Bibman.Pandoc (Citation (..), Format (extension, fromExtension), From, Reference, To, addReferences, emptyReferences, getReferences, mergePandocs, readPandoc, readPandocFrom, refToCitation, setCitekey, setReferences, writePandocTo)
import Control.Monad (forM_, when)
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import Data.Text qualified as T
import System.Environment (getEnv)
import System.FilePath ((</>))
import System.IO qualified as S
import Text.Pandoc (Pandoc)

-- | 'retry' by editing the entry in an editor
retry ::
  AppConfig -> -- config
  Bool -> -- whether to download
  Bool -> -- whether to not retry on failure
  Text -> -- content
  FilePath -> -- path relative to bibliography
  [FilterFunc] -> -- filters
  From -> -- input format
  To -> -- output format
  IO ()
retry config dl noretry content path fns input output = do
  tmp <- writeTempFile content
  editor <- getEnv "EDITOR"
  res <- openWith (editor <> " " <> tmp) True
  case res of
    Left e -> dieWithMsg e
    Right _ -> addToBib config dl noretry True (T.pack tmp) path fns input output

-- | downloads a entry from doi.org given a DOI
-- NOTE: hardcoded to PDF
download ::
  Bool -> -- whether to download
  AppConfig -> -- config
  Text -> -- doi
  FilePath -> -- filename
  FilePath -> -- path to store it to
  IO ()
download dl config doi filename path =
  when dl $
    case doi of
      "" -> dieWithMsg "could not find doi for entry"
      _ -> do
        p <- doi2pdf config doi
        isfile <- bibIsFile (bibPath config)
        case p of
          Left e -> dieWithMsg $ show e
          Right x ->
            if isfile
              then writeFileBinary config (filename <> ".pdf") x
              else writeFileBinary config (path </> filename <> ".pdf") x

add ::
  AppConfig -> -- config
  Bool -> -- whether to download
  FilePath -> -- path relative to bibliography
  [FilterFunc] -> -- filters
  To -> -- output format
  [Reference] -> -- references to add
  Pandoc -> -- pandoc to add to
  IO ()
add config dl path fns output refs pandoc = do
  isfile <- bibIsFile (bibPath config)
  let toadd = map f refs

  _ <-
    if isfile
      then do
        files <- ls config (bibPath config)
        res <- mapM readPandoc files
        let refs' = map (\(_, _, r) -> r) toadd
            updatedPandoc = setReferences pandoc refs'
            filtered' = applyFilters' fns updatedPandoc
        case eithersToEither res of
          Right pandocs -> do
            let output' = fromExtension (bibPath config)
            let bibliography = mergePandocs pandocs
            let added = addReferences bibliography (getReferences filtered')
            written <- writePandocTo output' added
            case written of
              Right t -> writeFileUTF8 config path t
              Left err -> dieWithMsg err
          Left err -> dieWithMsg err
      else do
        forM_ toadd $ \(_, filename, ref) -> do
          let updatedPandoc = setReferences pandoc [ref]
          written <- applyFilters fns output updatedPandoc
          case written of
            Right t -> writeFileUTF8 config (path </> filename <> extension output) t
            Left err -> dieWithMsg err

  forM_ toadd $ \(citation, filename, _) -> do
    download dl config (fromMaybe "" (doi citation)) filename path
  where
    f :: Reference -> (Citation, String, Reference)
    f ref = (citation, filename, updated)
      where
        citation = refToCitation ref
        ck = generateCitekey config citation
        filename = T.unpack ck
        updated = setCitekey ref ck

-- | reads multiple entries from STDIN and adds them to the bibliography
addFromStdin ::
  AppConfig -> -- config
  Bool -> -- whether to download the resource
  FilePath -> -- path relative to bibliography
  [FilterFunc] -> -- filters
  From -> -- input format
  To -> -- output format
  IO ()
addFromStdin config dl path fns input output = do
  content <- readFromStdin
  entry <- readPandocFrom input content
  case entry of
    Left err -> dieWithMsg err
    Right pandoc -> do
      if emptyReferences pandoc
        then dieWithMsg "Empty references. Maybe you forgot to set the --from format"
        else do
          let refs = getReferences pandoc
          add config dl path fns output refs pandoc

-- | reads entries (from a file or via doi.org) and adds them to the bibliography
addToBib ::
  AppConfig -> -- config
  Bool -> -- whether to download
  Bool -> -- whether to not retry on failure
  Bool -> -- whether the entry is added from a file or STDIN
  Text -> -- DOI
  FilePath -> -- path relative to bibliography
  [FilterFunc] -> -- filters
  From -> -- input format
  To -> -- output format
  IO ()
addToBib config dl noretry fromFile doiString path fns input output = do
  case doiString of
    "-" -> addFromStdin config dl path fns input output
    _ -> do
      content <- (if fromFile then readFileUTF8 (T.unpack doiString) else doi2bib doiString)
      entry <- readPandocFrom input content
      case entry of
        Left err ->
          if noretry
            then dieWithMsg err
            else
              ( do
                  putStrLn err
                  putStr "do you want to edit (y/n): "
                  S.hFlush S.stdout
                  res <- getLine
                  case res of
                    "y" -> retry config dl noretry content path fns input output
                    _ -> dieWithMsg "aborted by user"
              )
        Right pandoc -> do
          let refs = getReferences pandoc
          add config dl path fns output refs pandoc

-- File Name: FS.hs
-- Description: Bibliography directory stuff
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 10 Jan 2024 18:17:06
-- Last Modified: 29 Aug 2024 14:22:15

module Bibman.CLI.FS (ls, tree, writeFileUTF8, writeFileBinary, readFileUTF8, getResource, writeTempFile, bibIsFile) where

import Bibman.CLI.Config (AppConfig (bibPath, resourceFormats))
import Bibman.CLI.Sys (dieWithMsg, printDebug)
import Bibman.Pandoc (Format (..), fromFormats)
import Control.Monad (filterM, forM_, when)
import Data.ByteString qualified as B
import Data.List (sort)
import Data.Text (Text)
import Data.Text qualified as T
import Data.Text.IO qualified as TIO
import Data.Traversable (forM)
import System.Directory (canonicalizePath, createDirectoryIfMissing, doesDirectoryExist, doesFileExist, getTemporaryDirectory, listDirectory)
import System.FilePath (takeDirectory, takeExtension, takeFileName, (<.>), (</>))
import System.IO (IOMode (WriteMode), hClose, hSetEncoding, openTempFile, utf8, withBinaryFile, withFile)

bibIsFile :: FilePath -> IO Bool
bibIsFile path = do
  exists <- doesFileExist path
  if exists
    then pure True
    else do
      exists' <- doesDirectoryExist path
      if exists'
        then pure False
        else do
          dieWithMsg ("File does not exist: " <> path)
          pure False

-- | returns filepaths to all the files in a directory
listFiles :: FilePath -> IO [FilePath]
listFiles dir' = do
  contents <- listDirectory dir'
  paths <- forM contents $ \entry -> do
    let path = dir' </> entry
    isDir <- doesDirectoryExist path
    if isDir
      then listFiles path
      else do
        if takeExtension entry `elem` map extension fromFormats
          then return [path]
          else return []
  return (concat paths)

-- | returns the path to the resource associated with a given bibliography entry
-- the resource should be in the same path as the bibliography file
-- and have the citekey as its filename.
getResource :: AppConfig -> FilePath -> Text -> IO (Maybe FilePath)
getResource config filePath ck = do
  let formats = resourceFormats config
  let directory = takeDirectory filePath
  let tryFormat format = do
        let newFilePath = directory </> (T.unpack ck <.> T.unpack format)
        exists <- doesFileExist newFilePath
        return $ if exists then Just newFilePath else Nothing
  paths <- mapM tryFormat formats
  return $ foldr (\path acc -> case path of Just p -> Just p; _ -> acc) Nothing paths

-- | writes a unique temporary file which is not deleted
writeTempFile :: Text -> IO FilePath
writeTempFile content = do
  dir <- getTempDir
  (path, handle) <- openTempFile dir "bibman.bib"
  hSetEncoding handle utf8
  TIO.hPutStr handle content
  hClose handle
  pure path
  where
    getTempDir :: IO FilePath
    getTempDir = getTemporaryDirectory >>= canonicalizePath

-- | writes a resource to the bibliography (path relative to bibliography)
writeFileUTF8 :: AppConfig -> FilePath -> Text -> IO ()
writeFileUTF8 config path content = do
  isfile <- bibIsFile bib
  let newpath = if isfile then if path == "" then bib else takeDirectory bib </> path else bib </> path
  let parent = takeDirectory newpath
  createDirectoryIfMissing True parent
  withFile newpath WriteMode $ \handle -> do
    hSetEncoding handle utf8
    TIO.hPutStr handle content
    hClose handle
  printDebug $ "saved " <> path
  where
    bib = bibPath config

-- | writes a resource (binary) to the bibliography (path relative to bibliography)
writeFileBinary :: AppConfig -> FilePath -> B.ByteString -> IO ()
writeFileBinary config path content = do
  isfile <- bibIsFile bib
  let newpath = if isfile then if path == "" then bib else takeDirectory bib </> path else bib </> path
  let parent = takeDirectory newpath
  createDirectoryIfMissing True parent
  withBinaryFile newpath WriteMode $ \handle -> do
    B.hPut handle content
    hClose handle
  printDebug $ "saved " <> path
  where
    bib = bibPath config

readFileUTF8 :: FilePath -> IO Text
readFileUTF8 = TIO.readFile

-- | lists the bibliography directory recursively (path relative to bibliography)
ls :: AppConfig -> FilePath -> IO [FilePath]
ls config dir = do
  isfile <- bibIsFile bib
  if isfile
    then pure [bib]
    else listFiles (bib </> dir)
  where
    bib = bibPath config

-- | pretty prints the bibliography tree (similar to unix's tree command)
-- | (path relative to bibliography)
tree :: AppConfig -> FilePath -> IO ()
tree config dir = do
  isfile <- bibIsFile bib
  if isfile
    then putStrLn bib
    else treeRec (bib </> dir) ""
  where
    bib = bibPath config

    treeRec :: FilePath -> Text -> IO ()
    treeRec path indentP = do
      contents <- listDirectory path
      let bibs = sort $ filter (\name -> takeExtension name `elem` map extension fromFormats) contents
      dirs <- sort <$> filterM (\name -> doesDirectoryExist $ path </> name) contents

      forM_ (zip [1 ..] bibs) $ \(i, name) -> do
        let fullPath = path </> name
        isDir <- doesDirectoryExist fullPath
        let prefix = if i == length bibs then "└── " else "├── "
        let indent = indentP <> if i == length bibs then "    " else "│   "
        TIO.putStrLn $ indentP <> prefix <> T.pack name
        when isDir $ treeRec fullPath indent

      forM_ (zip [1 ..] dirs) $ \(i, d) -> do
        let name = takeFileName d
        let fullPath = path </> d
        let prefix = if i == length dirs then "└── " else "├── "
        let indent = indentP <> if i == length dirs then "    " else "│   "
        TIO.putStrLn $ indentP <> prefix <> T.pack name
        treeRec fullPath indent

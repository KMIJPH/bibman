-- File Name: Filter.hs
-- Description: Filter module
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 27 Mai 2024 15:19:34
-- Last Modified: 29 Aug 2024 11:54:55

module Bibman.CLI.Filter (applyFilters, applyFilters', toFilterFunc, Filter (..), FilterFunc) where

import Bibman.CLI.Config (AppConfig, Filter (..), readConfig)
import Bibman.Citekey (generateCitekey)
import Bibman.Pandoc (To, metaToText, refToCitation, standardizeSeparator, updateReferences, writePandocTo)
import Data.Map (Map)
import Data.Map qualified as M
import Data.Text (Text)
import Text.Pandoc (MetaValue (MetaString), Pandoc)

type FilterFunc = Map Text MetaValue -> Map Text MetaValue

toFilterFunc :: Filter -> IO FilterFunc
toFilterFunc AccessNote = pure accessed
toFilterFunc FixDate = pure fixDate
toFilterFunc NewCitekey = do citekey <$> readConfig
toFilterFunc ReplaceEmpty = pure emptyType
toFilterFunc ToWebpage = pure webpage

-- | applies filters
applyFilters' :: [FilterFunc] -> Pandoc -> Pandoc
applyFilters' fns p = foldl updateReferences p fns

-- | applies filters and writes the references to text
applyFilters :: [FilterFunc] -> To -> Pandoc -> IO (Either String Text)
applyFilters fns format p = writePandocTo format (applyFilters' fns p)

-- | renames 'accessed' fields to 'note' fields
accessed :: FilterFunc
accessed m = case acc of
  Nothing -> m
  Just x -> do
    let withNote = M.insert "note" (MetaString $ "Accessed on: " <> metaToText x) m
    M.delete "accessed" withNote
  where
    acc = M.lookup "accessed" m

-- | replaces the 'id' field with a new citekey
citekey :: AppConfig -> FilterFunc
citekey conf m = M.insert "id" (MetaString (generateCitekey conf citation)) m
  where
    citation = refToCitation m

-- | replaces empty types
emptyType :: FilterFunc
emptyType m = case typ of
  Just (MetaString "") -> M.insert "type" (MetaString "document") m
  _ -> m
  where
    typ = M.lookup "type" m

-- | standardizes date to YYYY-MM-DD
fixDate :: FilterFunc
fixDate m = case issued of
  Just (MetaString x) -> M.insert "issued" (MetaString $ standardizeSeparator x) m
  _ -> m
  where
    issued = M.lookup "issued" m

-- | converts entry types to 'wegpage' based on the occurrence of the `accessed` field
webpage :: FilterFunc
webpage m = case acc of
  Nothing -> m
  Just _ -> M.insert "type" (MetaString "webpage") m
  where
    acc = M.lookup "accessed" m

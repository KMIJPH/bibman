-- File Name: Download.hs
-- Description: Resource downloading
-- Docs: https://hackage.haskell.org/package/http-client
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 10 Jan 2024 18:54:02
-- Last Modified: 29 Aug 2024 16:05:22

module Bibman.CLI.Download (doi2pdf, doi2bib, extractUrl) where

import Bibman.CLI.Config (AppConfig (dlProvider))
import Bibman.CLI.Sys (printDebug)
import Control.Applicative ((<|>))
import Control.Exception (SomeException, try)
import Data.ByteString qualified as B
import Data.ByteString.Lazy.Char8 (toStrict)
import Data.Text (Text)
import Data.Text qualified as T
import Data.Text.Encoding qualified as TE
import Network.HTTP.Client qualified as C
import Network.HTTP.Client.TLS qualified as T
import Network.HTTP.Types (Header)
import Network.URI (escapeURIString, isUnescapedInURI)
import Text.Parsec
  ( anyChar,
    between,
    char,
    many,
    noneOf,
    optional,
    parse,
    string,
  )
import Text.Parsec qualified as P
import Text.Parsec.String (Parser)

userAgent :: Header
userAgent = ("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:50.0) Gecko/20100101 Firefox/50.0")

trim :: [Char] -> Text -> Text
trim what = f . f
  where
    f = T.reverse . T.dropWhile (`elem` what)

-- | requests a BibTeX entry using a doi string
-- e.g:
-- doi2bib "10.2134/agronmonogr47.c23"
doi2bib :: Text -> IO Text
doi2bib doi = do
  res <- req (fixUrl ("https://doi.org/" <> doi)) [("Accept", "application/x-bibtex")]
  return $ trim [' ', '\n', '\t'] $ TE.decodeUtf8 res

-- | tries to download a pdf resource (WIP)
-- wget -q -O
doi2pdf ::
  AppConfig -> -- config
  Text -> -- doi
  IO (Either String B.ByteString)
doi2pdf config doi = do
  let dlp = dlProvider config
  body <- htmlBody config doi
  case extractUrl body of
    Nothing -> pure $ Left "couldnt find a resource"
    Just url -> do
      let pdfUrl = trim ['/', ' '] url
      res <- try $ req (fixUrl pdfUrl) [("Accept", "application/pdf"), userAgent] :: IO (Either SomeException B.ByteString)
      case res of
        Left _ -> do
          let absolute = relativeToAbsolute dlp pdfUrl
          printDebug $ "downloading from " <> T.unpack absolute
          res2 <- req (fixUrl absolute) [("Accept", "application/pdf"), userAgent]
          pure $ Right res2
        Right r -> do
          printDebug $ "downloading from " <> T.unpack pdfUrl
          pure $ Right r
  where
    relativeToAbsolute :: Text -> Text -> Text
    relativeToAbsolute host url =
      if host `T.isPrefixOf` url
        then url
        else host <> "/" <> url

-- | GET request
req ::
  Text -> -- url
  [Header] -> -- headers
  IO B.ByteString
req url h = do
  manager <- C.newManager T.tlsManagerSettings
  let initial = C.parseRequest_ (T.unpack url)
  let re =
        initial
          { C.method = "GET",
            C.requestHeaders = h
          }
  res <- C.httpLbs re manager
  pure $ toStrict $ C.responseBody res

-- | returns the htmlBody from the request BIB_DL/doi
-- curl -s
htmlBody ::
  AppConfig -> -- config
  Text -> -- doi
  IO Text
htmlBody config doi = do
  let dlp = dlProvider config
  res <- req (fixUrl (dlp <> "/" <> doi)) [("Accept", "text/html"), ("Accept", "application/xhtml+xml")]
  pure $ TE.decodeUtf8 res

fixUrl :: Text -> Text
fixUrl input =
  if "https://" `T.isPrefixOf` input
    then url
    else "https://" <> url
  where
    url = T.pack $ escapeURIString isUnescapedInURI (T.unpack input)

----------------------------------------------------------------------------------------------------------------------------

-- | tries to extract the url to the resource from the html response
-- NOTE: very simple for now.. might need to work on this later
extractUrl :: Text -> Maybe Text
extractUrl body =
  case parse url "" (T.unpack body) of
    Left _ -> Nothing
    Right [] -> Nothing
    Right [u] -> Just u
    Right (u : _) -> Just u
  where
    url :: Parser [Text]
    url = filter (/= "") <$> many (P.try srcId <|> other')
      where
        srcId :: Parser Text
        srcId = do
          _ <- string "src="
          url' <- between (char '"') (char '"') (many $ noneOf ['"'])
          _ <- optional $ many (char ' ')
          _ <- string "id=\"pdf\""
          pure $ T.pack url'

        other' :: Parser Text
        other' = do
          _ <- anyChar
          pure ""

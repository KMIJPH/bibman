-- File Name: Sys.hs
-- Description: System stuff
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 15 Jan 2024 12:34:52
-- Last Modified: 29 Aug 2024 11:55:29

module Bibman.CLI.Sys (openWith, dieWithMsg, printDebug, readFromStdin) where

import Data.ByteString (hGetContents)
import Data.ByteString.Char8 (unpack)
import Data.Text (Text)
import Data.Text.IO qualified as TIO
import GHC.IO.Exception (ExitCode (ExitFailure, ExitSuccess))
import Prettyprinter (Pretty (pretty), annotate)
import Prettyprinter.Render.Terminal (Color (Red, Yellow), color, hPutDoc, putDoc)
import System.Exit (exitWith)
import System.IO (stderr)
import System.Process (CreateProcess (std_err, std_in, std_out), StdStream (CreatePipe, Inherit), createProcess, shell, waitForProcess)

-- | runs a 'open with' command
openWith ::
  String -> -- command
  Bool -> -- whether to run in terminal or not
  IO (Either String ())
openWith command terminal = do
  (_, _, Just stderr', process) <-
    ( if terminal
        then
          createProcess
            (shell command)
              { std_err = CreatePipe,
                std_in = CreatePipe,
                std_out = Inherit
              }
        else
          createProcess
            (shell command)
              { std_err = CreatePipe,
                std_in = Inherit,
                std_out = CreatePipe
              }
      )
  exitCode <- waitForProcess process
  case exitCode of
    ExitSuccess -> pure $ Right ()
    ExitFailure code -> do
      output <- hGetContents stderr'
      pure $ Left $ "exit code: " <> show code <> " -> " <> unpack output

dieWithMsg :: String -> IO ()
dieWithMsg msg = do
  hPutDoc stderr colored
  exitWith (ExitFailure 1)
  where
    colored = annotate (color Red) (pretty $ "[ERROR]: " <> msg)

printDebug :: String -> IO ()
printDebug msg = putDoc colored
  where
    colored = annotate (color Yellow) (pretty $ "[INFO]: " <> msg <> "\n")

readFromStdin :: IO Text
readFromStdin = TIO.getContents

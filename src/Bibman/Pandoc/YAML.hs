-- File Name: YAML.hs
-- Description: CSL yaml conversion (could not find it in pandoc)
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 10 Jul 2024 13:41:57
-- Last Modified: 12 Jul 2024 18:49:21

module Bibman.Pandoc.YAML (yamlToJson, jsonToYaml) where

import Data.Aeson (ToJSON, Value, eitherDecode, encode, fromJSON, object, toJSON, (.=))
import Data.Aeson.Types qualified as AT
import Data.ByteString.Lazy qualified as BL
import Data.Map (Map)
import Data.Map qualified as M
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
import Data.Yaml qualified as YAML

type DecodedYAML = Either YAML.ParseException YAML.Value

type DecodedJSON = Either String Value

type Reference = Map Text Value

type DateParts = Map Text [[Float]]

type DateArray = [Map Text Float]

-- | applies a funcion to all date variable fields
-- (https://github.com/citation-style-language/schema/blob/master/schemas/input/csl-data.json)
allFields :: (Text -> a -> a) -> a -> a
allFields f = f "accessed" . f "available-date" . f "event-date" . f "issued" . f "original-date" . f "submitted"

-- | converts a CSL YAML citation to CSL JSON (to the best of my knowledge)
-- unwraps the 'references' key and returns the value
-- converts 'date variable' map to a 'date-parts' array
yamlToJson :: Text -> Either String Text
yamlToJson content = case YAML.decodeEither' (encodeUtf8 content) :: DecodedYAML of
  Left err -> Left $ show err
  Right value -> do
    case fromJSON value :: AT.Result (Map Text [Reference]) of
      AT.Success m -> case unwrapRef m of
        Right unwrapped -> do
          let fixed = map (allFields toDateParts) unwrapped
          Right $ decodeUtf8 $ BL.toStrict $ encode fixed
        Left err -> Left $ "[YAMLTOJSON]: " <> err
      AT.Error err -> Left $ "[YAMLTOJSON]: " <> err
  where
    unwrapRef :: Map Text v -> Either String v
    unwrapRef m = case M.lookup "references" m of
      Just m' -> Right m'
      Nothing -> Left "Could not find key 'references'"

    toDateParts :: Text -> Reference -> Reference
    toDateParts k m = case M.lookup k m of
      Just obj -> case fromJSON obj :: AT.Result DateArray of
        AT.Success x -> M.insert k (toJSON $ M.fromList [("date-parts" :: Text, map mapToDateParts x)]) m
        _ -> m
      _ -> m

    mapToDateParts :: Map Text Float -> [Int]
    mapToDateParts m' = case M.lookup "year" m' of
      Just y -> case M.lookup "month" m' of
        Just m -> case M.lookup "day" m' of
          Just d -> [round y, round m, round d]
          _ -> [round y, round m]
        _ -> [round y]
      _ -> []

-- | converts a CSL JSON citation to CSL YAML (to the best of my knowledge)
-- wraps the references value in the 'references' key
-- converts the 'date-parts' array to a 'date variable' map
jsonToYaml :: BL.ByteString -> Either String BL.ByteString
jsonToYaml jsonContent = case eitherDecode jsonContent :: DecodedJSON of
  Right v -> case fromJSON v :: AT.Result [Reference] of
    AT.Success arr -> do
      let fixed = map (allFields fromDateParts) arr
      Right $ BL.fromStrict $ YAML.encode (wrapRef fixed)
    AT.Error err -> Left $ "[JSONTOYAML]: " <> err
  Left err -> Left $ "[JSONTOYAML]: " <> err
  where
    wrapRef :: (ToJSON a) => a -> Value
    wrapRef value = object ["references" .= value]

    fromDateParts :: Text -> Map Text YAML.Value -> Map Text YAML.Value
    fromDateParts k m = case M.lookup k m of
      Just obj -> case fromJSON obj :: AT.Result DateParts of
        AT.Success m' -> case M.lookup "date-parts" m' of
          Just arr -> M.insert k (toJSON $ map datePartsToMap arr) m
          Nothing -> m
        AT.Error _ -> m
      _ -> m

    datePartsToMap :: [Float] -> Map Text Int
    datePartsToMap arr = case arr of
      [y] -> M.fromList [("year", round y)]
      [y, m] -> M.fromList [("year", round y), ("month", round m)]
      [y, m, d] -> M.fromList [("year", round y), ("month", round m), ("day", round d)]
      _ -> M.empty

-- File Name: Citation.hs
-- Description: Pandoc / CSL JSON citation
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 06 Jul 2024 11:08:14
-- Last Modified: 29 Aug 2024 16:06:29

module Bibman.Pandoc.Citation
  ( Citation (..),
    issuedToYear,
    authorEditorPublisher,
    refToCitation,
  )
where

import Bibman.Pandoc.Date (issuedToYear)
import Bibman.Pandoc.Meta (Reference, metaToText)
import Bibman.Pandoc.Name (Name, metaToNames, namesToText)
import Control.Applicative ((<|>))
import Data.Map qualified as M
import Data.Maybe (fromMaybe)
import Data.Text (Text)

-- | convenience data type for pandoc's CSL JSON
-- https://github.com/citation-style-language/schema/blob/master/schemas/input/csl-data.json
-- NOTE: used for citekey generation and TUI stuff
data Citation = Citation
  { title :: Maybe Text,
    year :: Maybe Text,
    authors :: Maybe [Name],
    editors :: Maybe [Name],
    publisher :: Maybe Text,
    container :: Maybe Text,
    doi :: Maybe Text,
    url :: Maybe Text,
    citekey :: Maybe Text
  }
  deriving (Eq, Show)

refToCitation :: Reference -> Citation
refToCitation n =
  Citation
    { title = metaToText <$> M.lookup "title" n,
      year = issuedToYear <$> M.lookup "issued" n,
      authors = metaToNames <$> M.lookup "author" n,
      editors = metaToNames <$> M.lookup "editor" n,
      publisher = metaToText <$> M.lookup "publisher" n,
      container = metaToText <$> M.lookup "container-title" n,
      doi = metaToText <$> (M.lookup "doi" n <|> M.lookup "DOI" n),
      url = metaToText <$> (M.lookup "url" n <|> M.lookup "URL" n),
      citekey = metaToText <$> M.lookup "id" n
    }

-- | returns author, else editor, else publisher
authorEditorPublisher :: Citation -> Text
authorEditorPublisher Citation {..}
  | not (null a) = namesToText a
  | not (null e) = namesToText e
  | otherwise = fromMaybe "" publisher
  where
    a = fromMaybe [] authors
    e = fromMaybe [] editors

-- File Name: Name.hs
-- Description: Pandoc / CSL JSON name variable
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 06 Jul 2024 11:22:16
-- Last Modified: 15 Jul 2024 21:05:59

module Bibman.Pandoc.Name (Name (..), nameToText, namesToText, metaToName, metaToNames) where

import Bibman.Pandoc.Meta (metaToText)
import Data.Map qualified as M
import Data.Text (Text)
import Data.Text qualified as T
import Text.Pandoc (MetaValue (MetaList, MetaMap))

-- | CSL JSON name variable
-- | https://github.com/citation-style-language/schema/blob/master/schemas/input/csl-data.json
data Name = Name
  { family :: Maybe Text,
    given :: Maybe Text,
    dropping :: Maybe Text,
    nonDropping :: Maybe Text,
    suffix :: Maybe Text,
    commaSuffix :: Maybe Text,
    staticOrdering :: Maybe Text,
    literal :: Maybe Text,
    parseNames :: Maybe Text
  }
  deriving (Show, Eq)

metaToName :: MetaValue -> Name
metaToName (MetaMap m) =
  Name
    { family = metaToText <$> M.lookup "family" m,
      given = metaToText <$> M.lookup "given" m,
      dropping = metaToText <$> M.lookup "dropping-particle" m,
      nonDropping = metaToText <$> M.lookup "non-dropping-particle" m,
      suffix = metaToText <$> M.lookup "suffix" m,
      commaSuffix = metaToText <$> M.lookup "comma-suffix" m,
      staticOrdering = metaToText <$> M.lookup "static-ordering" m,
      literal = metaToText <$> M.lookup "literal" m,
      parseNames = metaToText <$> M.lookup "parse-names" m
    }
metaToName _ =
  Name
    { family = Nothing,
      given = Nothing,
      dropping = Nothing,
      nonDropping = Nothing,
      suffix = Nothing,
      commaSuffix = Nothing,
      staticOrdering = Nothing,
      literal = Nothing,
      parseNames = Nothing
    }

metaToNames :: MetaValue -> [Name]
metaToNames (MetaList l) = map metaToName l
metaToNames _ = []

-- | the representation of a single name used to filter in the TUI
nameToText :: Name -> Text
nameToText (Name {literal = Just lit}) = lit
nameToText Name {given = g, dropping = d, nonDropping = nd, family = f, suffix = s} =
  T.intercalate " " $
    filter (not . T.null) [maybeToText g, maybeToText d, maybeToText nd, maybeToText f, maybeToText s]

-- | the representation of names used in the citekey and filename
-- | 1 author = author1
-- | 2 authors = author1 & author 2
-- | >2 authors = author1 et al.
namesToText :: [Name] -> Text
namesToText authors =
  case authors of
    [] -> "no author"
    [Name {literal = Just lit}] -> lit
    [Name {dropping = d, nonDropping = nd, family = f}] ->
      T.intercalate " " $
        filter (not . T.null) [maybeToText d, maybeToText nd, maybeToText f]
    [a, b] -> namesToText [a] <> " & " <> namesToText [b]
    (x : _) -> namesToText [x] <> " et al."

maybeToText :: Maybe Text -> Text
maybeToText (Just t) = t
maybeToText Nothing = ""

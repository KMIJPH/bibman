-- File Name: IO.hs
-- Description: IO stuff
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 06 Jul 2024 15:12:53
-- Last Modified: 15 Jul 2024 15:45:35

module Bibman.Pandoc.IO
  ( From (..),
    To (..),
    Format (..),
    readPandocFrom,
    readPandoc,
    writePandocTo,
    fromFormats,
    toFormats,
  )
where

import Bibman.Pandoc.YAML (jsonToYaml, yamlToJson)
import Data.ByteString.Lazy qualified as BL
import Data.List (intercalate)
import Data.Text (Text)
import Data.Text qualified as T
import Data.Text.Encoding qualified as TE
import Data.Text.IO qualified as TIO
import System.FilePath (takeExtension)
import Text.Pandoc
  ( Pandoc,
    def,
    readBibLaTeX,
    readBibTeX,
    readCslJson,
    readEndNoteXML,
    readRIS,
    renderError,
    runIO,
    writeBibLaTeX,
    writeBibTeX,
    writeCslJson,
  )

class Format a where
  -- | reads format from extension (won't work with mimetype)
  fromExtension :: FilePath -> a

  -- | format to extension
  extension :: a -> String

  -- | format to string (CLI arg)
  formatStr :: a -> String

  -- | CLI arg to Format
  parse :: String -> Either String a

-- | https://pandoc.org/MANUAL.html#option--from
data From = Fbibtex | Fbiblatex | Fcsljson | Fcslyaml | Fendnote | Fris deriving (Eq, Show, Enum, Bounded)

instance Format From where
  fromExtension str = case takeExtension str of
    ".bibtex" -> Fbibtex
    ".yaml" -> Fcslyaml
    ".json" -> Fcsljson
    ".xml" -> Fendnote
    ".ris" -> Fris
    _ -> Fbiblatex
  extension a = case a of
    Fbibtex -> ".bibtex"
    Fbiblatex -> ".bib"
    Fcsljson -> ".json"
    Fcslyaml -> ".yaml"
    Fendnote -> ".xml"
    Fris -> ".ris"
  formatStr a = case a of
    Fbibtex -> "bibtex"
    Fbiblatex -> "biblatex"
    Fcsljson -> "csljson"
    Fcslyaml -> "cslyaml"
    Fendnote -> "endnote"
    Fris -> "ris"
  parse a = case a of
    "biblatex" -> Right Fbiblatex
    "bibtex" -> Right Fbibtex
    "csljson" -> Right Fcsljson
    "cslyaml" -> Right Fcslyaml
    "endnote" -> Right Fendnote
    "ris" -> Right Fris
    _ -> Left $ "Available formats: " <> intercalate ", " (map formatStr fromFormats)

fromFormats :: [From]
fromFormats = [minBound ..]

-- | https://pandoc.org/MANUAL.html#option--to
data To = Tbibtex | Tbiblatex | Tcsljson | Tcslyaml deriving (Eq, Show, Enum, Bounded)

instance Format To where
  fromExtension str = case takeExtension str of
    ".bibtex" -> Tbibtex
    ".yaml" -> Tcslyaml
    ".json" -> Tcsljson
    _ -> Tbiblatex
  extension a = case a of
    Tbibtex -> ".bibtex"
    Tbiblatex -> ".bib"
    Tcsljson -> ".json"
    Tcslyaml -> ".yaml"
  formatStr a = case a of
    Tbibtex -> "bibtex"
    Tbiblatex -> "biblatex"
    Tcsljson -> "csljson"
    Tcslyaml -> "cslyaml"
  parse a = case a of
    "biblatex" -> Right Tbiblatex
    "bibtex" -> Right Tbibtex
    "csljson" -> Right Tcsljson
    "cslyaml" -> Right Tcslyaml
    _ -> Left $ "Available formats: " <> intercalate ", " (map formatStr toFormats)

toFormats :: [To]
toFormats = [minBound ..]

-- | reads a pandoc document from string
readPandocFrom :: From -> Text -> IO (Either String Pandoc)
readPandocFrom format content = do
  let content' = case format of
        Fcslyaml -> yamlToJson content
        _ -> Right content
  case content' of
    Left err -> pure $ Left err
    Right c -> do
      res <- runIO $ f def c
      case res of
        Left err -> pure $ Left $ T.unpack (renderError err)
        Right p -> pure $ Right p
  where
    f = case format of
      Fbibtex -> readBibTeX
      Fbiblatex -> readBibLaTeX
      Fris -> readRIS
      Fcsljson -> readCslJson
      Fcslyaml -> readCslJson
      Fendnote -> readEndNoteXML

-- | reads a pandoc document from a file
readPandoc :: FilePath -> IO (Either String Pandoc)
readPandoc path = do
  content <- TIO.readFile path
  let from = fromExtension path :: From
  readPandocFrom from content

-- | writes a pandoc document to string
writePandocTo :: To -> Pandoc -> IO (Either String Text)
writePandocTo format pandoc = do
  res <- runIO $ f def pandoc
  case res of
    Right t -> case format of
      Tcslyaml -> case jsonToYaml (BL.fromStrict (TE.encodeUtf8 t)) of
        Right yaml -> pure $ Right (TE.decodeUtf8 $ BL.toStrict yaml)
        Left err -> pure $ Left err
      _ -> pure $ Right t
    Left err -> pure $ Left (T.unpack $ renderError err)
  where
    f = case format of
      Tbibtex -> writeBibTeX
      Tbiblatex -> writeBibLaTeX
      Tcsljson -> writeCslJson
      Tcslyaml -> writeCslJson

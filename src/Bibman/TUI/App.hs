{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE TemplateHaskell #-}

-- File Name: App.hs
-- Description: TUI
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 13 Jan 2024 17:46:21
-- Last Modified: 18 Jul 2024 12:00:30

module Bibman.TUI.App (app) where

import Bibman.CLI.Config (AppConfig (colOrder, colWidths))
import Bibman.CLI.Config qualified as C
import Bibman.TUI.Data
  ( AppState (..),
    FileAction (..),
    Row (..),
    authorAttr,
    columnAlignments,
    columnWidths,
    errorAttr,
    headerAttr,
    headerRow,
    highlightAttr,
    markedAttr,
    normalAttr,
    normalAttrRev,
    theMap,
    yearAttr,
  )
import Bibman.TUI.Events
  ( exportEvent,
    filterEvent,
    helpEvent,
    mainEvent,
    runCommandEvent,
    sortEvent,
  )
import Brick (Widget, get, getVtyHandle, hBox, str, txt, vBox, withAttr, (<+>), (<=>))
import Brick.Main qualified as M
import Brick.Types qualified as T
import Brick.Widgets.Border qualified as B
import Brick.Widgets.Center qualified as C
import Brick.Widgets.List qualified as L
import Brick.Widgets.Table qualified as Table
import Control.Monad.IO.Class (MonadIO (liftIO))
import Data.Text (Text)
import Data.Vector qualified as Vec
import Graphics.Vty qualified as V
import Lens.Micro ((^.))
import Lens.Micro.Mtl ((.=))
import Lens.Micro.TH (makeLensesFor)

makeLensesFor
  [ ("_config", "config"),
    ("_doiUrlWithInput", "doiUrlWithInput"),
    ("_doiUrlWithPrompt", "doiUrlWithPrompt"),
    ("_editWithInput", "editWithInput"),
    ("_editWithPrompt", "editWithPrompt"),
    ("_errorMessage", "errorMessage"),
    ("_exportInput", "exportInput"),
    ("_exportPrompt", "exportPrompt"),
    ("_fileWithInput", "fileWithInput"),
    ("_fileWithPrompt", "fileWithPrompt"),
    ("_filterInput", "filterInput"),
    ("_filterPrompt", "filterPrompt"),
    ("_helpText", "helpText"),
    ("_keybindList", "keybindList"),
    ("_showHelp", "showHelp"),
    ("_sortPressed", "sortPressed"),
    ("_terminalWidth", "terminalWidth"),
    ("_view", "view")
  ]
  ''AppState

app :: M.App AppState e ()
app =
  M.App
    { M.appDraw = drawUI,
      M.appChooseCursor = M.showFirstCursor,
      M.appHandleEvent = appEvent,
      M.appStartEvent = setWidth,
      M.appAttrMap = const theMap
    }

drawUI :: AppState -> [Widget ()]
drawUI st = if _showHelp st then [helpPopup] else [mainUI]
  where
    mainUI :: Widget ()
    mainUI = C.vCenter $ vBox [C.hCenter box, prompt, showKey, showErrorMessage]
      where
        list = st ^. view
        totalRows = Vec.length $ list ^. L.listElementsL
        width = st ^. terminalWidth
        conf = st ^. config
        order = colOrder conf
        widths = columnWidths width (colWidths conf)
        currentIdx = list ^. L.listSelectedL
        currentStr = maybe "-" (show . (+ 1)) currentIdx
        label = str $ "bibman " <> currentStr <> " / " <> show totalRows
        box =
          B.borderWithLabel label $
            drawElement False headerRow
              <=> L.renderList drawElement True list
        prompt
          | st ^. exportPrompt = withAttr highlightAttr $ txt "export to: " <+> withAttr normalAttr (txt (st ^. exportInput))
          | st ^. filterPrompt = withAttr highlightAttr $ txt "filter: " <+> withAttr normalAttr (txt (st ^. filterInput))
          | st ^. fileWithPrompt = withAttr highlightAttr $ txt "open with: " <+> withAttr normalAttr (txt (st ^. fileWithInput))
          | st ^. editWithPrompt = withAttr highlightAttr $ txt "edit with: " <+> withAttr normalAttr (txt (st ^. editWithInput))
          | st ^. doiUrlWithPrompt = withAttr highlightAttr $ txt "open url/doi with: " <+> withAttr normalAttr (txt (st ^. doiUrlWithInput))
          | otherwise = txt ""

        showKey :: Widget ()
        showKey = maybe (txt "") (withAttr highlightAttr . txt) $ st ^. helpText

        showErrorMessage :: Widget ()
        showErrorMessage = withAttr errorAttr $ txt $ st ^. errorMessage

        drawElement :: Bool -> Row -> Widget ()
        drawElement sel i@Row {..} =
          if i == headerRow
            then withAttr headerAttr $ hBox $ Table.alignColumns (columnAlignments order) widths header
            else withAttr normalAttr $ hBox $ Table.alignColumns (columnAlignments order) widths row
          where
            m = if marked then withAttr markedAttr $ txt "*" else txt " "
            header = [m] <> map headerOrder order
            row = [m] <> map columnOrder order

            headerOrder :: C.Order -> Widget ()
            headerOrder C.Author = txt a
            headerOrder C.Year = txt yr
            headerOrder C.Title = txt ti
            headerOrder C.Container = txt cn
            headerOrder C.Publisher = txt pb

            columnOrder :: C.Order -> Widget ()
            columnOrder C.Author = if sel then txt a else withAttr authorAttr $ txt a
            columnOrder C.Year = if sel then txt yr else withAttr yearAttr $ txt yr
            columnOrder C.Title = if sel then withAttr normalAttrRev $ txt ti else withAttr normalAttr $ txt ti
            columnOrder C.Container = if sel then withAttr normalAttrRev $ txt cn else withAttr normalAttr $ txt cn
            columnOrder C.Publisher = if sel then txt pb else withAttr authorAttr $ txt pb

    helpPopup :: Widget ()
    helpPopup = ui
      where
        list = st ^. keybindList
        order = colOrder (st ^. config)
        box =
          B.borderWithLabel (txt "help") $
            withAttr highlightAttr (hBox [txt "Key bindings:"])
              <=> L.renderList drawElement True list
        ui = C.center box

        drawElement :: Bool -> (Text, Text) -> Widget ()
        drawElement _ (key, desc) = hBox $ Table.alignColumns (columnAlignments order) [20, 80] [txt key, txt desc]

appEvent :: T.BrickEvent () e -> T.EventM () AppState ()
appEvent (T.VtyEvent (V.EvResize _ _)) = setWidth
appEvent (T.VtyEvent event) = do
  st <- get
  if
    | st ^. doiUrlWithPrompt -> runCommandEvent DoiUrl event
    | st ^. editWithPrompt -> runCommandEvent Edit event
    | st ^. fileWithPrompt -> runCommandEvent Open event
    | st ^. exportPrompt -> exportEvent event
    | st ^. filterPrompt -> filterEvent event
    | st ^. showHelp -> helpEvent event
    | st ^. sortPressed -> sortEvent event
    | otherwise -> mainEvent event
appEvent _ = pure ()

setWidth :: T.EventM () AppState ()
setWidth = do
  vty <- getVtyHandle
  (displayWidth, _) <- liftIO $ V.displayBounds (V.outputIface vty)
  terminalWidth .= displayWidth

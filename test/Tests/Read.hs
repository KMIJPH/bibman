module Tests.Read (readSpec) where

import Bibman.Pandoc (readPandoc)
import Data.Either (isRight)
import Data.Functor ((<&>))
import Test.Hspec

readSuccess :: FilePath -> IO ()
readSuccess path = do
  res <- readPandoc path <&> isRight
  res `shouldBe` True

readSpec :: Spec
readSpec = do
  describe "read bib" $ do
    it "reads bib files" $ do
      readSuccess "test/data/ascmo-10-1-2024.bib"

  describe "read ris" $ do
    it "reads ris files" $ do
      readSuccess "test/data/ascmo-10-1-2024.ris"

  describe "read csl yaml" $ do
    it "reads csl yaml files" $ do
      readSuccess "test/data/example.yaml"

  describe "read csl json" $ do
    it "reads csl json files" $ do
      readSuccess "test/data/example.json"

  describe "read endnote" $ do
    it "reads endnote xml files" $ do
      readSuccess "test/data/example.xml"
